#include "msg.h"
#include "uart.h"
#include "audio/audio.h"
#include "music_player.h"
#include "task_common.h"
#include "warning_tone.h"
#include "rec_api.h"
#include "task_rec.h"
#include "task_rec_key.h"
#include "rec_api.h"
#include "string.h"
#include "wdt.h"
#include "audio/dac_api.h"
#include "ui/ui_api.h"
#include "fat_io.h"
#include "rec_ui.h"
#include "rec_play.h"
#include "led.h"
#include "task_oid.h"
#include "rec_file_op.h"


#define REC_TASK_DEBUG_ENABLE
#ifdef REC_TASK_DEBUG_ENABLE
#define rec_task_printf log_printf
#else
#define rec_task_printf(...)
#endif

#if MIC_REC_EN

RECORD_OP_API *rec_mic_api = NULL;

extern u32 get_oidcode(void);
extern u8 get_readflag(void);
void sin_tone_play(u16 cnt);
void record_mutex_init(void *priv)
{
}

void record_mutex_stop(void *priv)
{
    rec_exit(&rec_mic_api);
}

static tbool task_rec_mode_check(void **priv)
{
    rec_task_printf("task_rec_mode_check !!\n");

    //DEV_HANDLE *dev = *priv;
    u32 parm;
    u32 dev_status;
    tbool dev_cnt = 0;

    //check some device online

    if (*priv == NULL)
    {
        *priv = dev_get_fisrt(MUSIC_DEV_TYPE, DEV_ONLINE);
    }


    // printf("2music cur_use_dev:0x%x,0x%x\n", cur_use_dev, *priv);
    if (*priv == (void *)sd0)
    {
        puts("[PRIV SD0]\n");
    }
    else if (*priv == (void *)sd1)
    {
        puts("[PRIV SD1]\n");
    }
    else if (*priv == (void *)usb)
    {
        puts("[PRIV USB]\n");
    }
    else
    {
        puts("[PRIV CACHE]\n");
    }

    if (*priv == NULL)
    {
        return false;
    }
    else
    {
        return true;
    }

    //check specific device online
    return true;
}


static void *task_rec_init(void *priv)
{
    rec_task_printf("task rec init !!\n");

    init_lastandplay_recfile();

    if(TASK_ID_REC != (TASK_ID_TYPE)priv)
        tone_play(TONE_REC_MODE, 0);

    led_fre_set(C_BLED_ON_MODE);

    return NULL;
}

static void task_rec_exit(void **hdl)
{
    task_clear_all_message();
    rec_exit(&rec_mic_api);
    mutex_resource_release("record");
    mutex_resource_release("record_play");

    ui_close_rec();
    rec_task_printf("task_rec_exit !!\n");
}


static void task_rec_deal(void *p)
{
    int msg;
    int msg_error = MSG_NO_ERROR;
    ENC_STA state = ENC_STOP;
    tbool ret = true;

    printf("****************REC TSAK*********************\n");

    while (1)
    {

        clear_wdt();

        msg_error = task_get_msg(0, 1, &msg);



        if (task_common_msg_deal(NULL, msg) == false)
        {
            music_tone_stop();
            task_common_msg_deal(NULL, NO_MSG);
            return;
        }

        state= rec_get_enc_sta(rec_mic_api);

        if (msg == MSG_REC_START)
        {
            if (FALSE == is_cur_resource("record"))   //当前资源不属record所有，不允许录音，防止资源冲突
            {
                continue;
            }
            else if((state != ENC_STOP)) //已经在录音状态下， 按下开始录音键不响应
            {
                 continue;
            }
        }

        if(state == ENC_STOP)//录音结束时, 读点可以跳转OID.
        {
            if(turnto_oidmode_check()== true)
            {
                music_tone_stop();
                task_common_msg_deal(NULL, NO_MSG);
                return;
            }
        }
        else if(state == ENC_STAR)//正在录音时, 点读不跳转, 清楚数据,否则停止录音后会跳转OID.
        {
            if(get_readflag())
                get_oidcode();
        }


        rec_msg_deal_api(&rec_mic_api, msg); //rec_msg_deal(x,y) record 流程

        if (NO_MSG == msg)
        {
            continue;
        }

        switch (msg)
        {
        case MSG_PLAY_DELETE_FILE_TONE:
            tone_play(TONE_DELETE_FILE, 0);
            break;
        case MSG_REC_NOFILE:
            tone_play(TONE_REC_NOFILE, 0);
        break;

       case MSG_PLAY_SIN_TONE:
            //if((state == ENC_STOP) && (rec_paly_status() != MUSIC_DECODER_ST_PLAY))
            //    sin_tone_play(100);
        break;
       case SYS_EVENT_PLAY_SEL_END: //提示音结束
            rec_task_printf("RECORD_SYS_EVENT_PLAY_SEL_END\n");

            if(tone_var.idx == TONE_REC_START)
            {
                tone_var.idx = 0;
                tone_var.status = 0;
                task_post_msg(NULL, 1, MSG_REC_START);
            }
            else if(tone_var.idx == TONE_REC_MODE)
            {
                if(get_enter_is_exist_recfile() == 0)
                {
                    tone_play(TONE_REC_NOFILE, 0);
                }
                else
                {
                    tone_var.idx = 0;
                    tone_var.status = 0;
                    task_post_msg(NULL, 1, MSG_REC_PLAY);
                }
            }
            else if(tone_var.idx == TONE_DELETE_FILE)
            {
                tone_var.idx = 0;
                tone_var.status = 0;
                if(get_enter_is_exist_recfile() == 1)
                {

                    task_post_msg(NULL, 1, MSG_REC_PLAY);
                }
                else
                {
                    tone_play(TONE_REC_NOFILE, 0);
                }
            }
            else if(tone_var.idx == TONE_REC_END)
            {
                tone_var.idx = 0;
                tone_var.status = 0;

                task_post_msg(NULL, 1, MSG_REC_PLAY);
            }


        case MSG_REC_INIT:
            rec_task_printf("MSG_REC_INIT\n");
            dac_channel_on(DAC_DIGITAL_CH, FADE_ON);
            ui_open_rec(&rec_mic_api, sizeof(RECORD_OP_API **));
            dac_set_samplerate(48000, 0);//
            mutex_resource_apply("record", 3, record_mutex_init, record_mutex_stop, rec_mic_api);
            break;

        #if LCD_SUPPORT_MENU
        case MSG_ENTER_MENULIST:
            UI_menu_arg(MENU_LIST_DISPLAY, UI_MENU_LIST_ITEM);
            break;
        #endif

        case MSG_REC_START_TONE:
            if(state == ENC_STOP)
                tone_play(TONE_REC_START,0);
            break;

        case MSG_REC_START:
            if((state != ENC_STAR)&&(rec_mic_api!=NULL))
            {
                led_fre_set(C_BLED_SLOW_MODE);
            }

            break;
        case MSG_REC_STOP:
            led_fre_set(C_BLED_ON_MODE);

           if(state != ENC_STOP)
                tone_play(TONE_REC_END,0);


            break;
        case MSG_INPUT_NUMBER_END:
        case MSG_INPUT_TIMEOUT:
            get_input_number(NULL);
            break;

        default:
            break;
        }



    }
}

const TASK_APP task_rec_info =
{
    .skip_check = task_rec_mode_check,
    .init 		= task_rec_init,
    .exit 		= task_rec_exit,
    .task 		= task_rec_deal,
    .key 		= &task_rec_key,
};
#endif
