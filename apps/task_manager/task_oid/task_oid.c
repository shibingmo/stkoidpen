#include "sdk_cfg.h"
#include "task_common.h"
#include "task_oid_key.h"
#include "two_wire.h"
#include "msg.h"
#include "dac.h"
#include "clock.h"
#include "dev_mg_api.h"
#include "file_operate.h"
#include "dev_manage.h"
#include "music_player.h"
#include "resource_manage.h"
#include "task_oid.h"
#include "audio.h"
#include "fat_io.h"
#include "warning_tone.h"
#include "updata.h"
#include "oid_main_function.h"
#include "led.h"
#include "task_idle.h"
#include "common.h"
#if 1//OID_PEN_EN

#define OIDMP3_TASK_DEBUG_ENABLE
#ifdef OIDMP3_TASK_DEBUG_ENABLE
#define oidmp3_task_printf log_printf
#else
#define oidmp3_task_printf(...)
#endif

extern DEV_HANDLE get_emitter_dev(void);




//////////////////////////////////////////////////////////////////////////////////////////////////////////
const unsigned short REPEAT_TIMES_PARA[4] = {2,5,10,20};
unsigned long BookID;			//
unsigned long TotalDotReadNum;	//一个OID码对应的声音个数
unsigned long FristDotReadOID;	//第一个点读OID码
unsigned long MaxDotReadOID;	//最大的点读OID码

volatile unsigned long MP3DataStartAddr;//用于最终MP3 播放公共的变量
volatile unsigned long MP3DataLen;		//用于最终MP3 播放公共的变量

unsigned long GameDataStartAddr;		//游戏数据一级索引起始地址
unsigned long ComVoiceDataIndexStartAddr;//公共音数据索引起始地址

unsigned short InGameState;		//是否是游戏模式
unsigned short TotalGameCombNum;//游戏组合总数
unsigned short GameCombStartOID;//游戏组合起始OID码

extern OID_READ_DES  	DotReadOIDDes;	//一个点读OID码的关联数据
extern DATA_INDEX		GameIdx1;			//游戏一级索引
extern GAME_CELL	  GameCellDes;		//一个游戏单元信息
extern GAME_COMB	  GameCombDes;		//一个游戏组合信?

unsigned short QuestionUsed[MAX_ANSWER_NUM];//已经点过的游戏序列
unsigned short QuestionCnt;				//玩过的游戏个数
unsigned short AnswerErrCnt;			//答错的次数
short PlayOIDMp3;						//播放点读的声音
unsigned short RepeatReadCnt;			//复读计数
unsigned short RepeatReadMax=1;			//复读最大次数
unsigned long gOIDCodebk; //OID code ,used for all AP

unsigned short VoiceColumn;
u16 BookIdxInlstFile;
void sin_tone_play(u16 cnt);
u16 led_set;
//////////////////////////////////////////////////////////////////////////////////////////////////////////

u16 sys_index;

u32 Get_Bnf_Sound_StartAddr(void)
{
    return MP3DataStartAddr;
}

u32 Get_Bnf_Sound_EndAddr(void)
{
    return MP3DataStartAddr+MP3DataLen;
}

void clear_oid_play_addr(void)
{
    MP3DataStartAddr = 0;
    MP3DataLen = 0;
}

void clear_oid_open_bkinfo(void)
{
    MP3DataStartAddr = 0;
    MP3DataLen = 0;
    FristDotReadOID = 0x00;
    MaxDotReadOID = 0x00;
    BookIdxInlstFile = 0x00;

    RepeatReadCnt = 1;
}

u16 Get_RepeatReadMax(void)
{
    return RepeatReadMax;
}

void Set_RepeatReadMax(u8 flag)
{
    if(flag == 1)
    {
        RepeatReadMax =0xFFFF;
    }
    else
    {
        if(RepeatReadMax ==1)
        {
            RepeatReadMax = 2;
        }
        else if(RepeatReadMax == 2)
        {
            RepeatReadMax = 3;
        }
        else if(RepeatReadMax == 3 || (RepeatReadMax ==0xFFFF))
        {
            RepeatReadMax = 1;
        }
    }



    oidmp3_task_printf("set RepeatReadMax = %d\n", RepeatReadMax);
}

void Set_RepeatReadCnt(u16 cnt)
{
    RepeatReadCnt = cnt;
}

static void oidmp3_msg_filter(int *msg)
{
    switch (*msg)
    {
	    case MSG_MUSIC_NEXT_FILE:
	    case MSG_MUSIC_PREV_FILE:
	    case MSG_MUSIC_FF:
	    case MSG_MUSIC_FR:
	    case MSG_MUSIC_AB_RPT:
	    case MSG_HALF_SECOND:
	        *msg = NO_MSG;
	        break;
    }
}


static u32 oidmp3_polling_to_get_oidcode(void)
{
	return get_oidcode();//接收-解析笔头数据
}


u16 HandleOIDGame(_FIL_HDL *pOIDDataFile,u32 *InOIDNum)
{
	unsigned short i,j;
	unsigned short ThisGameUsed;
	unsigned short FindGameID;
	unsigned short Test;


	if (((*InOIDNum - OID_GAME_CODE_MIN) % 20) == 0) //game enter OID code
	{
		//enter game ,get the game comb description data
		memset(QuestionUsed,0,sizeof(QuestionUsed));//已经点过的游戏序列
		QuestionCnt = 0;	 		//玩过的游戏个数
		AnswerErrCnt = 0;

		if (GetGameCombDesData(pOIDDataFile, *InOIDNum)== 0)
		{
			if (GameCombDes.OID_ATTR == GAMECOMB_USER_SINGLE)
			{
				InGameState = GAME_WAIT_QUESTION;
			}
			else
			{
				GetGameCellDesData(pOIDDataFile, *InOIDNum - OID_GAME_CODE_MIN);
				InGameState = GAME_WAIT_ANSWER;
			}

		}
		else	//no Game Data
		{
			InGameState = GAME_OUT;
			return -1;
		}
	}
	else
	{
		if (InGameState == GAME_OUT)//非点读模式下
		{
			Test = (unsigned short)((*InOIDNum - OID_GAME_CODE_MIN) % 20);

			if(Test < 10) //如果是游戏问题码，提醒“请点击游戏开始”
			{
				GetSystemVoiceStartPosLenByIndex(pOIDDataFile,COMVOICE_GameMode);
                RepeatReadCnt = 1;
                task_post_msg(NULL, 1, MSG_MUSIC_PLAY);
				return 1;
			}
			else if (Test > 10)
			{
				if ((*InOIDNum < GameCombStartOID) || (*InOIDNum > (GameCombStartOID + TotalGameCombNum)))
				{
					return -5;	//不是数据中包含的游戏码段
				}
				if (GetGameCombDesData(pOIDDataFile,(*InOIDNum / 20)*20) != 0) //游戏数据错误
				{
					return -6;
				}

				if ((Test - 10) > GameCombDes.TotalGameNum)	//检查答案数量，如果超过答案数量
				{
					if(GetSystemVoiceStartPosLenByIndex(pOIDDataFile,COMVOICE_GameMode))
                    {
                        RepeatReadCnt = 1;
                        task_post_msg(NULL, 1, MSG_MUSIC_PLAY);
                        return 2;
                    }
				}
				else	//非游戏模式下需要发点读音
				{
					return 3;
				}
			}
		}

		if (InGameState == GAME_WAIT_QUESTION)	//提问
		{
			ThisGameUsed = 0;
			FindGameID = 0;

			if((*InOIDNum - OID_GAME_CODE_MIN) % 20 < 10)
			{
				for (i = 0;i < GameCombDes.TotalGameNum;i ++)	//搜索对应的问题编号
				{
					if (*InOIDNum == GameCombDes.QuestionOIDPool[i])
					{
						FindGameID = 1;
						break;
					}
				}
				if (FindGameID)
				{
					for (j = 0;j < QuestionCnt;j++ )	//查找是否是做过的题目
					{
						if (*InOIDNum == QuestionUsed[j])
						{
							ThisGameUsed = 1;
							break;
						}
					}
					if (ThisGameUsed)	//如果此问题已经做过，提示错误
					{
						if(GetSystemVoiceStartPosLenByIndex(pOIDDataFile,COMVOICE_GAME_CELL_ERR_GO))
                        {
                            RepeatReadCnt = 1;
                            task_post_msg(NULL, 1, MSG_MUSIC_PLAY);
                            return -2;
                        }
					}

					if (GetGameCellDesData(pOIDDataFile,i))
					{
						return -3;
					}
					else
					{
						InGameState = GAME_WAIT_ANSWER;
						QuestionUsed[QuestionCnt] = (unsigned short)*InOIDNum;	//记录做过的游戏
						QuestionCnt ++;
					}
				}
				else	//不是有效的问题码
				{
					return -4;
				}
			}
			else	//不是问题码
			{
				return -5;
			}
		}
		else if (InGameState == GAME_WAIT_ANSWER) //比较答案
		{
			switch (GameCombDes.OID_ATTR)
			{
				case GAMECOMB_USER_SINGLE:
					if (*InOIDNum == GameCellDes.AnswerPool[0])	//回答正确，此游戏类型只有答案 GameCellDes.AnswerPool[0]
					{
						if (QuestionCnt < GameCombDes.TotalGameNum)	//还有问题
						{
							InGameState = GAME_WAIT_QUESTION;
							AnswerErrCnt = 0;
							GetSystemVoiceStartPosLenByIndex(pOIDDataFile,COMVOICE_GAME_CELL_OK_GO);
						}
						else	//问题全部完成
						{
							InGameState = GAME_OUT;
							GetSystemVoiceStartPosLenByIndex(pOIDDataFile,COMVOICE_GAME_ALLOK_OVER);
						}

					}
					else	//答案错误
					{
						AnswerErrCnt ++;
						if (AnswerErrCnt < 3)
						{
							GetSystemVoiceStartPosLenByIndex(pOIDDataFile,COMVOICE_GAME_CELL_ERR_GO);
						}
						else
						{
							InGameState = GAME_OUT;
							GetSystemVoiceStartPosLenByIndex(pOIDDataFile,COMVOICE_GAME_CELL_ERR_OVER);
						}

					}
					break;
				case GAMECOMB_RAND_SINGLE:
					break;
				case GAMECOMB_ORDER_SINGLE:

					break;
				case GAMECELL_RAND_MULTI:
					break;
				case GAMECOMB_ORDER_MULTI:
					break;
				default:
					break;

			}
		}
	}

    RepeatReadCnt = 1;
    task_post_msg(NULL, 1, MSG_MUSIC_PLAY);
	*InOIDNum = -1;
	return 0;
}




static void oidmp3_dealwith_oidcode(FILE_OPERATE *fop, u32 oidcode)
{
    static u32 BookID;
    u32 Ret = 0, TmpBookID , Temp_FileIndex=0, FileIndex = 0;

    _FIL_HDL *pOIDDataFile = file_operate_get_file_hdl(fop);

    if(fop == NULL)
    {
        log_printf("oidmp3_dealwith_oidcode, fop =NULL.\n");
        return ;
    }

    if((oidcode != 0xFFFFFFFF) && (oidcode != 0x00))
    {
        log_printf("oidmp3_dealwith_oidcode, oidcode =0x%08x, first=%d, last=%d\n", oidcode, FristDotReadOID, MaxDotReadOID);

        if(oidcode == OID_GAME_STOP_CODE) //游戏结束码
        {
            if(InGameState != GAME_OUT)
            {
                InGameState = GAME_OUT;
                //播放游戏结束提示音
            }
        }
        else if((oidcode >= OID_GAME_CODE_MIN) && (oidcode < OID_GAME_CODE_MAX)) //游戏区间码
        {
            HandleOIDGame(pOIDDataFile, &oidcode);
        }
        else if((oidcode >= OID_SYS_MIN) && (oidcode <= OID_SYS_MAX)) //系统区间码
        {
            if ((oidcode >= OID_LANG_COL_START) && (oidcode <= OID_LANG_COL_END))	//语音切换
            {
                VoiceColumn = oidcode;
                sys_index= oidcode -OID_LANG_COL_START;
                if (GetSystemVoiceStartPosLenByIndex(pOIDDataFile,COMVOICE_SYS_voice+sys_index))
                {
                    RepeatReadCnt = 1;
                    task_post_msg(NULL, 1, MSG_MUSIC_PLAY);
                }
            }
            else if(DotReadModuleHandleSysOID(oidcode))//音量+-，暂停播放，快进快退等操作
            {

            }
        }
        else if((oidcode >= OID_DIY_REC_MIN) && (oidcode <= OID_DIY_REC_MAX)) //DIY 录音区间码
        {

        }
        else if((oidcode >= OID_mp3_CODE_MIN) && (oidcode <= OID_mp3_CODE_MAX))//播放MP3文件
        {

        }
        else if((oidcode >= FristDotReadOID) && (oidcode <= MaxDotReadOID)) //only dot read mode
        {
            music_decoder_stop_two();
            if (GetDotReadStartPosLenByOID(pOIDDataFile,oidcode,VoiceColumn - OID_LANG_COL_START))
            {
                 RepeatReadCnt = RepeatReadMax;
                 task_post_msg(NULL, 1, MSG_MUSIC_PLAY);
            }
            else
            {
                //不能识别，播放系统提示音
            }
        }
        else if((((oidcode < FristDotReadOID) || (oidcode > MaxDotReadOID)) && (FristDotReadOID != 0) ) \
                || (FristDotReadOID == MaxDotReadOID ) \
                || ((oidcode >= OID_BOOK_CODE_MIN ) && (oidcode <= OID_BOOK_CODE_MAX)) \
                )
        {
            music_decoder_stop_two();

            TmpBookID = GetOIDBookDataByOIDOrIdx(fop, &FileIndex,oidcode,&BookIdxInlstFile);

            if (TmpBookID )
            {
                BookID = TmpBookID;
                file_operate_set_file_number(fop, FileIndex);
                Ret = file_operate_op(fop, FOP_OPEN_FILE_BYNUM, NULL, NULL);
                if(!Ret)
                {
                    GetOIDDataHead(pOIDDataFile);
                    if((oidcode >= OID_BOOK_CODE_MIN ) && (oidcode <= OID_BOOK_CODE_MAX))
                    {
                        if (GetSystemVoiceStartPosLenByIndex(pOIDDataFile,COMVOICE_Book))
                        {
                            RepeatReadCnt = 1;
                            task_post_msg(NULL, 1, MSG_MUSIC_PLAY);
                        }
                        else //点读失败
                        {
                            if(get_emitter_dev()==NULL)
                            {
                                tone_play(TONE_READ_FAIL, 0);
                            }
                            else
                            {
                                task_post_msg(NULL, 1, MSG_OID_READ_FAIL);
                            }
                        }
                    }
                    else if((oidcode >= FristDotReadOID) && (oidcode <= MaxDotReadOID))
                    {
                        if (GetDotReadStartPosLenByOID(pOIDDataFile,oidcode,VoiceColumn - OID_LANG_COL_START))
                        {
                             RepeatReadCnt = RepeatReadMax;
                             task_post_msg(NULL, 1, MSG_MUSIC_PLAY);
                        }
                        else //点读失败
                        {
                            if(get_emitter_dev()==NULL)
                            {
                                tone_play(TONE_READ_FAIL, 0);
                            }
                            else
                            {
                                task_post_msg(NULL, 1, MSG_OID_READ_FAIL);
                            }
                        }
                    }
                    else //点读失败
                    {
                            if(get_emitter_dev()==NULL)
                            {
                                tone_play(TONE_READ_FAIL, 0);
                            }
                            else
                            {
                                task_post_msg(NULL, 1, MSG_OID_READ_FAIL);
                            }
                    }
                }
                else //点读失败
                {
                    if(get_emitter_dev()==NULL)
                    {
                        tone_play(TONE_READ_FAIL, 0);
                    }
                    else
                    {
                        task_post_msg(NULL, 1, MSG_OID_READ_FAIL);
                    }
                }
            }
            else   //打开书本失败，打开原来的书本  点读失败
            {
                if(get_emitter_dev()==NULL)
                {
                    tone_play(TONE_READ_FAIL, 0);
                }
                else
                {
                    task_post_msg(NULL, 1, MSG_OID_READ_FAIL);
                }

            }
        }
        else //OID code不在范围
        {

        }

    }

}


void oid_play_file_voice(MUSIC_PLAYER *obj, u32 filenum)
{
    if(obj == NULL)
    {
        log_printf("oid_play_file_voice, obj = NULL.\n");
        return ;
    }

    log_printf("fileNum:%d, start:0x%08x, end:0x%08x \n", filenum, Get_Bnf_Sound_StartAddr(), Get_Bnf_Sound_EndAddr());

    tone_var.status = 0;
    tone_var.idx = 0;
    oid_player_play_spec_num_file(obj, filenum);
}


tbool turnto_oidmode_check(void)//检测是否跳转到OID
{
    if(get_readflag())
    {
        log_printf("TURN to----OID MODE\n");
        task_switch(TASK_ID_OID, PASS_HI_TONE);//跳过模式提示音
        return TRUE;
    }


    return FALSE;
}

static void oid_player_mutex_init(void *priv)
{
    log_printf("oid_player_mutex_init in.\n");
    if(is_cur_resource("oidplayer"))
    {
        log_printf("oidplayeroidplayeroidplayeroidplayer\n");
    }
    else if(is_cur_resource("music")||is_cur_resource("music"))
    {
        log_printf("musicmusicmusicmusic\n");
    }

}

static void oid_player_mutex_stop(void *priv)
{
    log_printf("oid_player_mutex_stop:0x%x\n", priv);
    MUSIC_PLAYER *obj = priv;

    if (obj ) {
        music_player_destroy(&obj);
    }
}

MUSIC_PLAYER *oid_player_start(void)
{
    MUSIC_PLAYER *obj = NULL;
    if(is_cur_resource("oidplayer"))
    {
        mutex_resource_release("tone");
        mutex_resource_release("music");
    }

    obj = oid_player_creat();

    if (obj) {
       mutex_resource_apply("oidplayer", 3, oid_player_mutex_init, oid_player_mutex_stop, obj);
    }

    return obj;
}

static tbool task_oid_skip_check(void **priv)
{
	oidmp3_task_printf("%s in...\n", __func__);

    return true;
}

extern u32 os_time_get(void);
static void *task_oid_init(void *priv)
{
	oidmp3_task_printf("%s in...priv:0x%x \n", __func__,priv);
    MUSIC_PLAYER *obj = NULL;

   	dac_channel_on(MUSIC_CHANNEL, 0);
//    set_sys_vol(sound.vol.sys_vol_l, sound.vol.sys_vol_r, FADE_ON);
    audio_set_output_buf_limit(OUTPUT_BUF_LIMIT_MUSIC_TASK);

    led_fre_set(C_BLED_ON_MODE);
    fat_mem_init(NULL, 0);  //使用默认的RAM， overlay task

    oidmp3_task_printf("inininin :%d\n",os_time_get()*10);
    CreatOIDFileList();
    oidmp3_task_printf("outoutout :%d\n",os_time_get()*10);
    obj = oid_player_start();

	VoiceColumn  = OID_LANG_COL_START;

    if(TASK_ID_OID != (TASK_ID_TYPE)priv)
    {
         tone_play(TONE_OID_MODE, 0);
    }
    else if(get_BookFileCnt() == 0)
    {
        if((TASK_ID_OID != (TASK_ID_TYPE)priv) || (priv == IDLE_POWER_UP))
        tone_play(TONE_OID_NOFILE, 0);
    }


    set_receive_oidcode_enable();

    return obj;
}

static void task_oid_deal(void *hdl)
{
    oidmp3_task_printf("task_oidmp3_deal ~~~~~~~~~~~~\n");

    int msg = NO_MSG;
    u32 err, oidcode = 0xFFFFFFFF;
    u8 music_start = 0;
    tbool ret = true;
    MUSIC_PLAYER *obj = (MUSIC_PLAYER *)hdl;


    while (1)
     {
        task_get_msg(0, 1, &msg);




        if (get_tone_status()) {    //提示音还未播完前过滤涉及解码器操作的消息
            oidmp3_msg_filter(&msg);
        }

        if (task_common_msg_deal(obj, msg) == false) {

            return;
        }

        if(get_BookFileCnt() > 0)
        {
            oidcode = oidmp3_polling_to_get_oidcode();
            if(oidcode != 0xFFFFFFFF)
            {
                oidmp3_task_printf("player_type:%d \n", obj->player_type);
                if(obj->player_type != 2)//for reopen the book
                {
                    clear_oid_open_bkinfo();

                    obj = oid_player_creat();
                }

                oidmp3_dealwith_oidcode(obj->fop, oidcode);
            }
        }


        if (NO_MSG == msg) {
            continue;
        }


        switch (msg) {

        case MSG_MUSIC_PP:
            if((obj->player_type == 2) && (music_decoder_get_status(obj->dop) != MUSIC_DECODER_ST_STOP))
            {
                oidmp3_task_printf("OID Play <->puase.\n");
                music_player_pp(obj);
            }

            break;

        case MSG_MUSIC_PLAY:
            oid_play_file_voice(obj, BookIdxInlstFile);
            break;

        //case MSG_PLAY_SIN_TONE:
        //    if(obj->player_type != 3)
        //    if(music_player_get_status(obj)== MUSIC_DECODER_ST_PLAY)
        //        break;

       //     sin_tone_play(100);
            break;
        case MSG_VOL_DOWN:
        case MSG_VOL_UP:
       // if(obj->player_type != 3)
       //     if(music_player_get_status(obj)== MUSIC_DECODER_ST_PLAY)
       //         break;

      //  if ((sound.vol.sys_vol_l>0)&&(sound.vol.sys_vol_l <= get_max_sys_vol(0))) {
      //      tone_play(TONE_VOL_DOWN, 0);
      //  }

            break;
        case SYS_EVENT_DEC_FR_END:
        case SYS_EVENT_DEC_FF_END:
        case SYS_EVENT_DEC_END:
            music_decoder_stop(obj->dop);
            oidmp3_task_printf("SYS_EVENT_DEC_END, fun = %s, line = %d\n", __func__, __LINE__);

            if(RepeatReadMax>1)
            {
                if(--RepeatReadCnt)
                {
                    task_post_msg(NULL, 1, MSG_MUSIC_PLAY);
                }
            }

            break;

        case MSG_OID_REPEAT_SHORT:
            Set_RepeatReadMax(0);
            if(music_decoder_get_status(obj->dop) != MUSIC_DECODER_ST_PLAY)
            {
                if(Get_RepeatReadMax() == 1)
                {
                    tone_play(TONE_DING, 0);
                }
                else if(Get_RepeatReadMax() == 2)
                {
                    tone_play(TONE_DING2, 0);
                }
                else if(Get_RepeatReadMax() == 3)
                {
                    tone_play(TONE_DING3, 0);
                }
            }


            break;
        case MSG_OID_REPEAT_LONG:
            Set_RepeatReadMax(1);
            if(music_decoder_get_status(obj->dop) != MUSIC_DECODER_ST_PLAY)
            {
                tone_play(TONE_DING, 0);
            }
            break;
        case SYS_EVENT_DEC_DEVICE_ERR:
            puts("SYS_EVENT_DEC_DEVICE_ERR\n");
            break;

        case SYS_EVENT_PLAY_SEL_END:
            oidmp3_task_printf("SYS_EVENT_PLAY_TONE_END\n");
            if(tone_var.idx == TONE_OID_MODE)
            {
                if(get_BookFileCnt() == 0)
                {
                    tone_play(TONE_OID_NOFILE, 0);

                    break;
                }

            }
            else if(tone_var.idx == TONE_OID_NOFILE)
            {

            }

            {
                tone_var.status = 0;
                tone_var.idx    = 0;
                oidmp3_task_printf("player_type:%d \n", obj->player_type);

                if(obj->player_type != 2)//for reopen the book
                {
                    clear_oid_open_bkinfo();

                    obj = oid_player_creat();
                }
            }

            break;
        case MSG_MUSIC_AB_RPT:
            oidmp3_task_printf("MSG_MUSIC_AB_RPT\n");
            music_player_ab_repeat_switch(obj);
            break;

        case MSG_UPDATA:
            log_printf("MSG_UPDATA:%x\n", file_operate_get_dev(obj->fop));
            device_updata(sd0);
            break;
        case MSG_HALF_SECOND:

            break;

        default:
            break;
        }
    }
}


#if BT_EMITTER_EN

void emitter_oidmp3_thread(FILE_OPERATE *obj)
{
    oidmp3_dealwith_oidcode(obj, oidmp3_polling_to_get_oidcode());
}

static u8 emitter_oidmp3_flag = 0;
void set_emitter_oidmp3(u8 flag)
{
    emitter_oidmp3_flag = flag;
}

u8 is_emitter_oidmp3(void)
{
    return  emitter_oidmp3_flag;
}

void *emitter_oidmp3_mode_start(void *dev)
{
    MUSIC_PLAYER *obj = NULL;

    tbool ret = 0;

    set_emitter_oidmp3(1);

    CreatOIDFileList();

    obj = oid_player_creat();

   if(obj!= NULL)
   {
       set_receive_oidcode_enable();
   }

    return obj;
}

void emitter_oidmp3_mode_stop()//like as emitter_music_stop()
{
    set_emitter_oidmp3(0);
    clear_oid_open_bkinfo();

    set_receive_oidcode_disable();
}

#endif



static void task_oid_exit(void **hdl)
{
    oidmp3_task_printf("task_oidmp3_exit !!\n");
    /* dac_channel_off(MUSIC_CHANNEL, 0); */
    //cur_use_dev1 = music_player_get_cur_dev(*hdl);
    music_player_destroy((MUSIC_PLAYER **)hdl);

    mutex_resource_release("oidplayer");

    audio_set_output_buf_limit(OUTPUT_BUF_LIMIT);
    set_sys_freq(SYS_Hz);
    task_clear_all_message();

    get_readflag();//clean the last flag.
    FristDotReadOID = 0;
    MaxDotReadOID = 0;
    clear_oid_open_bkinfo();
    set_receive_oidcode_disable();
    puts("oidmp3_exit ok!\n");
}





const TASK_APP task_oidmp3_info = {
    .skip_check = NULL,//task_oid_skip_check,//
    .init 		= task_oid_init,
    .exit 		= task_oid_exit,
    .task 		= task_oid_deal,
    .key 		= &task_oid_key,
};

#endif // OID_PEN_EN
