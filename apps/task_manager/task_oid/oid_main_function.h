#ifndef OID_MAIN_FUNCTION_H_INCLUDED
#define OID_MAIN_FUNCTION_H_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////
typedef struct OID_READ_DES{
	unsigned short OID_Attr;		//add V0.3
	unsigned short VoiceNumPerOID;
	unsigned long VoiceDataOffset;
}OID_READ_DES;

//
typedef struct OID_GAME_DES{
	unsigned long OID_Num;
	unsigned long OID_Attr;
}OID_GAME_DES;

//
typedef struct DATA_INDEX{
	unsigned long IndexOffset;
	unsigned short IndexLength;
}DATA_INDEX;

typedef struct VOICE_DATA{
	unsigned long offset;
	unsigned long length;
}VOICE_DATA;



typedef struct GAME_CELL{
	unsigned long OIDAttr;						//逻辑OID属性
	VOICE_DATA	QuestionVoice;					//游戏题目音
	unsigned short StepPromptNum;				//游戏每步提示个数(对数)
	unsigned short AnswerNum;					//游戏答案个数
	unsigned long AnswerPool[MAX_ANSWER_NUM];	//游戏答案OID序列
	//VOICE_DATA	StepPromptVoicePool[MAX_ANSWER_NUM*2];//游戏每步对应的提示音//95300方案暂时省略

}GAME_CELL;

typedef struct GAME_COMB{
	unsigned long OID_ATTR;						//OID属性
	VOICE_DATA GamePromptVoice;					//进入游戏提示音
	unsigned short GameNumOneRound;				//一轮要回答的问题
	unsigned short TotalGameNum;					//游戏总数
	unsigned long QuestionOIDPool[MAX_CHILD_GAME_NUM];	//点码选游戏对应OID序列
	DATA_INDEX LogicGameIndexPool[MAX_CHILD_GAME_NUM];	//子游戏数据索引序列

}GAME_COMB;


OID_READ_DES  	DotReadOIDDes;	//一个点读OID码的关联数据
DATA_INDEX		GameIdx1;			//游戏一级索引
GAME_CELL	  GameCellDes;		//一个游戏单元信息
GAME_COMB	  GameCombDes;		//一个游戏组合信?

u8 get_BookFileCnt(void);
u8 checkOIDisInBook(u32 oidcode);
bool CreatOIDFileList(void);
u16 GetSystemVoiceStartPosLenByIndex(_FIL_HDL *pOIDDataFile, u16 SystemVoiceIndex);
u16 DotReadModuleHandleSysOID(u32 InputOIDCode);
u16 GetDotReadStartPosLenByOID(_FIL_HDL *pOIDDataFile,unsigned long OIDNum,unsigned short Column);
u16 GetOIDBookDataByOIDOrIdx(FILE_OPERATE *fop_api, u32 *pFileIndex,u32 BookOIDNum,u16 *BookIdx);
u16 GetOIDDataHead(_FIL_HDL *pOIDDataFile);
u16 GetGameCombDesData(_FIL_HDL *pOIDDataFile,unsigned long OIDNum);
u16 GetGameCellDesData(_FIL_HDL *pOIDDataFile,unsigned long LogicGameCellNum);
#endif // OID_MAIN_FUNCTION_H_INCLUDED
