#ifndef __TASK_OIDMP3_H__
#define __TASK_OIDMP3_H__
#include "typedef.h"
#include "task_manager.h"
//////////////////////////////////////////////////////////////////////////////////////////////////////////
#define OID_BOOK_CODE_MIN				60000	//选书码区间最小值
#define OID_BOOK_CODE_MAX				64000	//选书码区间最大值

#define OID_BOOK_CODE_MIN				60000	//选书码区间最小值
#define OID_BOOK_CODE_MAX				64000	//选书码区间最大值

#define OID_mp3_CODE_MIN				57001	//选书码区间最小值
#define OID_mp3_CODE_MAX				59998	//选书码区间最大值


#define OID_DIY_REC_MIN					55001 	//63001 start record OID code
#define OID_DIY_REC_MAX					63999	//63500 end OID code 64000
#define OID_DIY_1_REC_MIN				64001 	//63001 start record OID code
#define OID_DIY_1_REC_MAX				65000	//63500 end OID code 64000

#define OID_DIY_RECPLAY_MIN				64000 	//63501 start PLAY record file OID code
#define OID_DIY_RECPLAY_MAX				65000	//63500 PLAY record file the final OID code

#define OID_GAME_CODE_MIN				55000	//游戏区间码最小值
#define OID_GAME_CODE_MAX				56999	//游戏区间码最大值，实际为51999

#define OID_GAME_STOP_CODE				56999	//游戏结束码


#define OID_SYS_MIN		65001		//系统码最小值
#define OID_SYS_MAX		65530		//系统码最大值

#define OID_LANG_COL_START		65011		//系统码最小值
#define OID_LANG_COL_END		65020		//系统码最大值

#define DOTREAD_OID_INDEX_TAB_OFFSET	8	//点读数据OID码索引表开始偏移量，前面8个字节分别放点读个数及最小OID码
#define ONE_READ_OID_INDEX_CELL			8	//one read OID index index info
#define ONE_COM_VOICE_DATA_INDEX		8	//一个公共提示音索引长度


#define ONE_GAME_OID_FIRST_INDEX_LEN		6	//游戏一级索引数据单元长度
#define ONE_GAME_OID_INDEX_CELL				6	//one OID index index info
#define ONE_GAMECELL_ANSWERPOOL_OFFSET		16 //一个游戏单元中答案序列的起始偏移位置

#define HEADMSG_SIZE						512		//file head information
#define DOT_READ_START_POS					512		//点读数据起始位置

#define MULBOOK_NUM_OFFSET		126				//多个点读数据标识，>1代表多个数据
#define BOOK_OFFSET				128				//选书码
#define GAME_OFFSET				132				//游戏数据起始地址
#define COMMON_VOICE_OFFSET		136				//公共提示音索引起始位置

#define GAME_FIRST_INDEX_DATA_OFFSET	4		//游戏一级索引数据偏移位置，前面四个字节放游戏个数及第一个游戏码
#define GAMECOMB_USER_SINGLE_IDX1_POS	16		//点选游戏组合中用户点选OID码起始位置

//141 -- 382 //预留多个OID数据的索引表,每个索引8个字节

#define ENCRYPT_TAB_INDEX			382				//选取哪组加密表
#define ENCRYPT_CODE_POSITION		384				//加密匙放置位置，共16字节
#define ENCRYPT_CODE_LEN			128				//加密表共8组，一组16字节


#define MAX_ANSWER_NUM		16					//一个游戏支持的最多答案个数
#define MAX_CHILD_GAME_NUM	16			//一个游戏组合包含最多游戏子项个数
#define MAX_GAME_TYPE_NUM	11			//最大支持的游戏类型

//OID 属性定义
#define	OID_READONE			0x01	//点读
#define	OID_FOLLOW			0x02	//跟读
#define	OID_READALL			0x03	//整读
#define GAMECELL_SINGLE		0x04	//单选游戏单元
#define GAMECOMB_RAND_SINGLE	0x05//随机单选游戏组合
#define GAMECOMB_USER_SINGLE	0x06//自选单选游戏组合
#define GAMECOMB_ORDER_SINGLE	0x07//有序单选游戏组合
#define GAMECELL_RAND_MULTI		0x08//无序多选游戏单元
#define GAMECELL_ORDER_SINGLE	0x09//有序多选游戏单元
#define GAMECOMB_RAND_MULIT		0x0A//无序多选游戏组合
#define GAMECOMB_ORDER_MULTI	0x0B//有序多选游戏组合
#define OID_SHENGMU				0x0C//声母
#define OID_YUNMU				0x0D//韵母
#define OID_PINDUS				0x0E//拼读组合

//公共提示音索引位置
#define COMVOICE_PowerON					0  //开机音
#define COMVOICE_PowerOFF					1  //关机音
#define COMVOICE_LowBatt					2  //低电提示音
#define COMVOICE_Book						3  //书名提示音
#define COMVOICE_InitStart					4  //初始化提示音
#define	COMVOICE_InitFinish					5  //初始化结束提示音
#define COMVOICE_Waiting					6  //等待提醒提示音
#define COMVOICE_SystemErr					7  //系统错误提示音
#define COMVOICE_DiskFull					8  //磁盘满提示音
#define COMVOICE_NoMP3						9  //无MP3文件提示音
#define COMVOICE_NoOIDData					10 //无点读文件提示音
#define COMVOICE_InvalidePage				11 //无效页提示音
#define COMVOICE_InvalideDir				12 //无效目录提示音
#define COMVOICE_RecStart					13 //录音开始提示音
#define COMVOICE_RecStop					14 //录音结束提示音
#define COMVOICE_VolUp						15 //音量加提示音
#define COMVOICE_VolDown					16 //音量减提示音
#define COMVOICE_MaxVol						17 //最大音量提示音
#define COMVOICE_MinVol						18 //最小音量提示音
#define COMVOICE_OIDMode					19 //点读模式提示音
#define COMVOICE_MP3Mode					20 //MP3模式提示音
#define COMVOICE_FollowRead					21 //跟读模式提示音
#define COMVOICE_RecMode					22 //录音模式提示音
#define COMVOICE_StoryMode					23 //故事模式提示音
#define COMVOICE_GameMode					24 //游戏模式提示音
#define COMVOICE_NoGame						25 //无故事提示音
#define COMVOICE_FollowReadStart			26 //跟读开始提示音
#define COMVOICE_FollowReadStop				27 //跟读结束提示音
#define COMVOICE_Num_0						28 //数字0提示音
#define COMVOICE_EXITGAME					38 //游戏退出
#define COMVOICE_SYS_voice					40 //系统提示音

#define COMVOICE_GAME_CELL_OK_OVER				64 //单个游戏正确且游戏结束
#define COMVOICE_GAME_CELL_OK_GO				72 //单个游戏正确但游戏继续下一个
#define COMVOICE_GAME_CELL_ERR_OVER				80 //单个游戏错误且游戏结束
#define COMVOICE_GAME_CELL_ERR_GO				88 //单个游戏错误但游戏继续下一个
#define COMVOICE_GAME_CELL_WAIT					96 //游戏过程中超时等待用户作答
#define COMVOICE_GAME_ALLOK_OVER				104//全部游戏正确且游戏结束
#define COMVOICE_GAME_ALLERR_OVER				112//全部游戏错误且游戏结束
#define COMVOICE_GAME_CELL_OTHER_FIND			120//多个答案的游戏中还有其他答案
#define COMVOICE_GAME_ANSWER_FINDED				128//答案已经找过了
#define COMVOICE_GAME_OVER_RESULT				136//全部答题结束评分






//游戏中的状态
#define		GAME_OUT			0	//退出游戏
#define		GAME_WAIT_QUESTION	1	//等待提问
#define		GAME_WAIT_ANSWER	2	//等待作答

//////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PASS_HI_TONE      ((void *)1)
extern const TASK_APP task_oidmp3_info;
tbool turnto_oidmode_check(void);


unsigned short RepeatReadCnt;
u16 Get_RepeatReadMax();
void Set_RepeatReadMax(u8 flag);
void Set_RepeatReadCnt(u16 cnt);
#endif//__TASK_OIDMP3_H__

