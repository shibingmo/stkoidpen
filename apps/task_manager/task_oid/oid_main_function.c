#include "asm_type.h"
#include "dev_manage.h"
#include "fs_io.h"
#include "file_io.h"
#include "file_operate.h"
#include "file_op_err.h"
#include "sdk_cfg.h"
#include "music_player.h"
#include "task_oid.h"
#include "oid_main_function.h"

#define OID_MAIN_DEBUG_ENABLE
#ifdef OID_MAIN_DEBUG_ENABLE
#define oid_main_printf log_printf
#else
#define oid_main_printf(...)
#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////
extern unsigned long BookID;			//
extern unsigned long TotalDotReadNum;	//一个OID码对应的声音个数
extern unsigned long FristDotReadOID;	//第一个点读OID码
extern unsigned long MaxDotReadOID;	//最大的点读OID码

extern unsigned long MP3DataStartAddr;//用于最终MP3 播放公共的变量
extern unsigned long MP3DataLen;		//用于最终MP3 播放公共的变量
extern unsigned long GameDataStartAddr;		//游戏数据一级索引起始地址
extern unsigned long ComVoiceDataIndexStartAddr;//公共音数据索引起始地址

extern unsigned short InGameState;		//是否是游戏模式
extern unsigned short TotalGameCombNum;//游戏组合总数
extern unsigned short GameCombStartOID;//游戏组合起始OID码
u16 BookFileCnt;

extern u32 os_time_get(void);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

struct bookinfo
{
    u32 fileidx;
    u32 bookid;
    u32 minoidcode;
    u32 maxoidcode;
};

struct bookinfo booklist[60];

#define BOOK_OFFSET				128				//选书码

u8 get_BookFileCnt(void)
{
    return BookFileCnt;
}

bool checkpath_ext(char *fname, char const *ext)
{
    char *str = fname;
    u8   fname_len = 0;

    if (!ext || (fname == NULL))
    { //不匹配
        return false;
    }

   // while (*str++ != '\0') {
   //     fname_len ++;
   // }

   // oid_main_printf("fext_name = %s\n", fname + fname_len - 3);
    if (!memcmp(fname + strlen(fname) - 3, ext, 3)) {
        return true;
    }

    return false;
}

/* 扫描文件夹下所有bnf文件，并将文件的信息读出建立书本列表，m_BookFileList[]
以便在文件个数较多时，加快选书时间 */
char STKlist_name[18]  =   "/pen/STKLIST.BIN";
static u8 OIDCreatSTKList = 0;
u32 stk_first_file_num = 0xFFFFFFFF, stk_last_file_num = 0xFFFFFFFF;
u32 wirte_byte = 0;

bool CreatOIDFileList(void)
{
    oid_main_printf("CreatOIDFileList in ....\n");

    _FIL_HDL listhdl;

     u16 FatCache1[256];	//used for get FAT data
     u16 FatCache2[256];  //used for get FAT data
    u16 FileCnt = 0;
    char *f_path = NULL;
    u32 i = 0, j = 0, ret = 0;
    u32 WriteNum;
    u32  TmpBook=0, BookID;
    u32 TotalDotReadNum;
    u32 FristDotReadOID;
    u32 MaxDotReadOID;

    FILE_OPERATE *fop_api =NULL;

    fop_api = file_operate_creat();

    if (fop_api == NULL)
    {
        printf("creat fop_api err...\n");
        return false;
    }

    file_operate_set_dev(fop_api, (u32)sd0);
    ret = file_api_creat(fop_api->fop_file, (void *)fop_api->fop_info->dev, 0);
    if (ret == false)
    {
        printf("creat file_api err...\n");
        file_operate_destroy(&fop_api);
        return false;
    }

    if(&(fop_api->fop_file->fs_hdl) == NULL)
    {
        printf("fs_hdl err...\n");
        return false;
    }

    memset(&listhdl, 0x00, sizeof(listhdl));

    if(task_get_cur()== TASK_ID_PC)
        ret = fs_open(&(fop_api->fop_file->fs_hdl), &listhdl, (char *)STKlist_name, FA_CREATE_ALWAYS | FA_WRITE);
    else
        ret = fs_open(&(fop_api->fop_file->fs_hdl), &listhdl, (char *)STKlist_name, FA_CREATE_NEW | FA_WRITE);

    if (!ret)//创建列表
    {
        oid_main_printf("--create oidfilelist.bin success...\n");
        fs_close(&listhdl);
        fs_sync(&listhdl);

        fs_close(&(fop_api->fop_file->file_hdl));
        memset(&listhdl, 0x00, sizeof(listhdl));


        stk_first_file_num = 0xFFFFFFFF;
        stk_last_file_num = 0xFFFFFFFF;
        BookFileCnt = 0;
    }
    else
    {
        oid_main_printf("--create oidfilelist.bin fail or exist...\n");

        memset(FatCache2, 0, sizeof(FatCache2));
        fs_read(&listhdl, (u8* )FatCache2,  16);

        BookFileCnt = *((unsigned long *)&FatCache2[0]);

        fs_close(&listhdl);
        fs_sync(&listhdl);

            file_operate_set_file_ext(fop_api, "STK");
            file_operate_dev_bp(fop_api, NULL);

            file_operate_set_path(fop_api, (u8*)"/pen/", 0);
            ret = fs_get_file_bypath(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), (u8 *)fop_api->fop_info->filepath);
            if (!ret)
            {
                fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FOLDER_FILE, &stk_first_file_num, &stk_last_file_num);
                printf("stk_first_file_num=%d, stk_last_file_num :%d\n", stk_first_file_num, stk_last_file_num);
                printf("find stk_filenumber:%d\n", os_time_get()*10);
            }
            else
            {
                stk_first_file_num = 0xFFFFFFFF;
                stk_last_file_num = 0xFFFFFFFF;
                BookFileCnt = 0;
            }

        file_operate_destroy(&fop_api);
        return true;
    }

    extern void delay_2ms(u32 delay_time);
    delay_2ms(5);


    file_operate_set_file_ext(fop_api, "STK");
    file_operate_dev_bp(fop_api, NULL);

    file_operate_set_path(fop_api, (u8*)"/pen/", 0);
    ret = fs_get_file_bypath(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), (u8 *)fop_api->fop_info->filepath);

    oid_main_printf("--ret: %d\n", ret);

    if (!ret)
    {
        fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FOLDER_FILE, &stk_first_file_num, &stk_last_file_num);
        printf("stk_first_file_num=%d, stk_last_file_num :%d\n", stk_first_file_num, stk_last_file_num);
        printf("stk_find filenumber:%d\n", os_time_get()*10);
        fs_close(&(fop_api->fop_file->file_hdl));
        fs_sync(&(fop_api->fop_file->file_hdl));

        WriteNum = 16;
        memset(FatCache1, 0, sizeof(FatCache1));

        ((unsigned long*)FatCache1)[0] = stk_last_file_num -  stk_first_file_num + 1;
        ((unsigned long*)FatCache1)[1] = 0xFFFFFFFF; //无用乱码
        ((unsigned long*)FatCache1)[2] = 0xFFFFFFFF; //无用乱码
        ((unsigned long*)FatCache1)[3] = 0xFFFFFFFF; //无用乱码

        BookFileCnt = stk_last_file_num -  stk_first_file_num + 1;

        _FS_HDL  *fs_p = file_operate_get_fs_hdl(fop_api);
        _FIL_HDL *f_p = file_operate_get_file_hdl(fop_api);


        for(i = 0; i < BookFileCnt; i++)
        {
            fs_get_file_byindex(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), i+stk_first_file_num);


            fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FILE_NAME, &f_path);
            printf("\ni = %d, f_path:%s\n",i,  f_path);
            if(checkpath_ext(f_path, "STK"))
            {

                memset(FatCache2, 0, sizeof(FatCache2));
                fs_seek(f_p, 0, 0);
                fs_read(f_p, (u8* )FatCache2,  sizeof(FatCache2));
                BookID = FatCache2[BOOK_OFFSET/2];

                memset(FatCache2, 0, sizeof(FatCache2));
                fs_seek(f_p, 0, HEADMSG_SIZE);
                fs_read(f_p, (u8* )FatCache2,  sizeof(FatCache2));

                TotalDotReadNum = *((unsigned long *)&FatCache2[0]);
                FristDotReadOID = *((unsigned long *)&FatCache2[2]);
                MaxDotReadOID = FristDotReadOID + TotalDotReadNum;

                ((unsigned long*)FatCache1)[WriteNum/4] = i;
                ((unsigned long*)FatCache1)[WriteNum/4+1] = BookID;
                ((unsigned long*)FatCache1)[WriteNum/4+2] = FristDotReadOID;
                ((unsigned long*)FatCache1)[WriteNum/4+3] = MaxDotReadOID;

                if(TmpBook == BookID)//now no use
                {
                    ((unsigned long*)FatCache1)[1] = TmpBook;
                }

                WriteNum += 16;

                if((WriteNum == 512) || ((i+stk_first_file_num) == stk_last_file_num))
                {
                    fs_close(f_p);
                    fs_sync(f_p);

                    memset(&listhdl, 0x00, sizeof(listhdl));
                    ret = fs_open(&(fop_api->fop_file->fs_hdl), &listhdl, (char *)STKlist_name, FA_WRITE);
                    if (!ret)
                    {
                        fs_seek(&listhdl , 0, wirte_byte);
                        ret = fs_write(&listhdl, (u8* )FatCache1, WriteNum);
                        printf("ret  = %d, fs_tell:%d \n", ret, fs_tell(&listhdl));
                        wirte_byte += fs_tell(&listhdl);
                        fs_sync(&listhdl);
                        fs_close(&listhdl);

                        WriteNum=0;
                        memset(&listhdl, 0x00, sizeof(listhdl));
                        memset(FatCache1, 0, sizeof(FatCache1));
                        memset(FatCache2, 0, sizeof(FatCache2));
                    }
                }
            }
            else
            {
                    fs_close(f_p);
            }
        }

    }
    else
    {
        //文件夹内为空/
        printf("no stk file OR no pen folder\n");
    }

    file_operate_destroy(&fop_api);

    return true;

}

u8 checkOIDisInBook(u32 oidcode)
{
    u8 i;

    for(i = 0; i < get_BookFileCnt(); i++)
    {
        if(booklist[i].fileidx != 0)
        {
            if(oidcode == booklist[i].bookid)
                return 1;
            else if((oidcode >= booklist[i].minoidcode) && (oidcode <= booklist[i].maxoidcode))
                return 1;
        }
    }

    return 0;
}


//找OID对应的STK文件，返回选书码
u16 GetOIDBookDataByOIDOrIdx(FILE_OPERATE *fop_api, u32 *pFileIndex,u32 BookOIDNum,u16 *BookIdx)
{
	unsigned long FileCount;
	unsigned long TotalSector;
	unsigned long i,j;
	unsigned long TmpBookOIDNum;
	unsigned long TemFristDotReadOID, TemMaxDotReadOID;
	unsigned short Offset;
	unsigned long index;
	u16 idex = 0;
    u16 FatCache1[256];
    u32 ret = 0;

//1.先关闭原来打开的文件
	_FIL_HDL *pFile = file_operate_get_file_hdl(fop_api);
	if (pFile == NULL)
    {
        oid_main_printf("pFile == NULL...line=%d\n", __LINE__);
    }

    fs_close(pFile);

//2.打开BIN文件列表
    ret = fs_open(&(fop_api->fop_file->fs_hdl), pFile, (char *)STKlist_name, FA_WRITE);
    if (ret != FILE_OP_NO_ERR)
	{
        oid_main_printf("open OIDLIST.BIN fail......\n");
		return 0;
	}

//3.获取STK文件个数
	memset(FatCache1, 0, sizeof(FatCache1));
	fs_read(pFile, (u8* )FatCache1, sizeof(FatCache1));
	FileCount = ((unsigned long*)FatCache1)[0];
	TotalSector = ((FileCount + 1) * 16) / 512;

//4.获取数据
	Offset = 0;
	i = 0;
	if (BookOIDNum == 0xffffffff)	//按文件索引取数据
	{
		if (*BookIdx > FileCount)//more than the max book number
		{
			*BookIdx = 1;
			Offset = 16;
			TmpBookOIDNum = ((unsigned long*)FatCache1)[Offset/4+1]; //选书码
			*pFileIndex = stk_first_file_num + ((unsigned long*)FatCache1)[Offset/4]; //文件索引

			fs_seek(pFile,0,0);
			((unsigned long*)FatCache1)[1] = TmpBookOIDNum; //本次选中的书码
			Offset = fs_write(pFile, (u8 *)FatCache1, sizeof(FatCache1));
			fs_close(pFile);
			return TmpBookOIDNum;
		}
		else
		{
			fs_read(pFile, (u8* )FatCache1, (*BookIdx + 1) * 16 );
			TmpBookOIDNum = ((unsigned long*)FatCache1)[1]; //选书码
			*pFileIndex = stk_first_file_num + ((unsigned long*)FatCache1)[0]; //文件索引

			fs_seek(pFile, 0, 0);
			fs_read(pFile, (u8* )FatCache1, sizeof(FatCache1));
			((unsigned long*)FatCache1)[1] = TmpBookOIDNum; //本次选中的书码
			fs_seek(pFile, 0, 0);
			Offset = fs_write(pFile, (u8* )FatCache1, sizeof(FatCache1));
			fs_close(pFile);
			return TmpBookOIDNum;
		}
	}
	else	//按OID 号取数据
	{
		//*BookIdx = 1;
		if (BookOIDNum == 0)	//默认第一个数据
		{
			TmpBookOIDNum = ((unsigned long*)FatCache1)[1]; //上次保留选书码
			if (TmpBookOIDNum == 0xffffffff)
			{
				Offset = 16;
				TmpBookOIDNum = stk_first_file_num + ((unsigned long*)FatCache1)[Offset/4+1]; //选书码
				*pFileIndex = stk_first_file_num + ((unsigned long*)FatCache1)[Offset/4]; //文件索引

				fs_seek(pFile,0,0);
				((unsigned long*)FatCache1)[1] = TmpBookOIDNum; //本次选中的书码
				Offset = fs_write(pFile, (u8 *)FatCache1, sizeof(FatCache1));
				fs_close(pFile);
				*BookIdx = TmpBookOIDNum; //默认第一个
				return TmpBookOIDNum;
			}
			else
			{
				BookOIDNum = stk_first_file_num + TmpBookOIDNum;	//get book id that is saved by last time
				return BookOIDNum;
			}
		}
        else
        {       oid_main_printf("BookOIDNum:%d\n", BookOIDNum);
                idex = 0;
                while (1)
                {
                        fs_seek(pFile, 0, (idex + 1) * 16);
                        oid_main_printf("fs_tell:%d\n", fs_tell(pFile));
                        fs_read(pFile, (u8 *)FatCache1, 16);
                        put_buf((u8 *)FatCache1, 16);
                        TmpBookOIDNum = ((unsigned long*)FatCache1)[1]; //选书码
                        TemFristDotReadOID = ((unsigned long*)FatCache1)[2]; //最小码
                        TemMaxDotReadOID = ((unsigned long*)FatCache1)[3]; //最大码

                        oid_main_printf("index:%d, TmpBookOIDNum = %d,  TemFristDotReadOID = %d,  TemMaxDotReadOID = %d\n", ((unsigned long*)FatCache1)[0], TmpBookOIDNum,TemFristDotReadOID,TemMaxDotReadOID);

                        if (idex > FileCount)//more than the max book number
                        {
                            break;
                        }
                        else if((BookOIDNum == TmpBookOIDNum) || ((BookOIDNum>=TemFristDotReadOID) && (BookOIDNum <= TemMaxDotReadOID)))
                        {
                            *pFileIndex = stk_first_file_num+((unsigned long*)FatCache1)[0]; //文件索引

                            //fs_seek(pFile,0,0);
                            //fs_read(pFile, (u8 *)FatCache1, sizeof(FatCache1));
                            //((unsigned long*)FatCache1)[1] = *pFileIndex; //本次选中的书码
                            //fs_seek(pFile,0,0);
                            //Offset = fs_write(pFile, (u8 *)FatCache1, sizeof(FatCache1));
                            //oid_main_printf("fun = %s, line=%d ,Offset=%d\n", __func__,__LINE__, Offset);
                            (*BookIdx) = *pFileIndex;
                            fs_close(pFile);
                            return TmpBookOIDNum;
                        }
                        idex++;
                }
        }

	}
	fs_close(pFile);
	return 0;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//Function: DotReadModuleHandleSysOID
//Input:	u32 InputOIDCode
//Output:	void
//Description:
//		Handle system OID in dot read module
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
u16 DotReadModuleHandleSysOID(u32 InputOIDCode)
{
	u16  rec =1;


	switch(InputOIDCode)
	{
        default:
            rec = 0;
            break;
	}

	return rec;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//Function:GetOIDDataHead
//Input:FS_FILE *pOIDDataFile
//Output:u16  BookID  ComVoiceDataIndexStartAddr  GameDataStartAddr
//Description:
//		get all data include in head,include gamedata start address BookID ...
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
u16 GetOIDDataHead(_FIL_HDL *pOIDDataFile)
{
	unsigned short tmp;
	unsigned long RdSectNum;
	unsigned long Offset;
    u16 FatCache1[256];

    oid_main_printf("fun = %s, line=%d\n", __func__,__LINE__);

    memset(FatCache1, 0, sizeof(FatCache1));
	tmp = fs_seek(pOIDDataFile,0,0);
	tmp = fs_read(pOIDDataFile, (u8* )FatCache1, sizeof(FatCache1));
	if (0 == tmp)
	{
		return 0;
	}

	BookID = *((unsigned long *)&FatCache1[BOOK_OFFSET/2]);
	GameDataStartAddr = *((unsigned long *)&FatCache1[GAME_OFFSET/2]); 	//GameData start addr
	ComVoiceDataIndexStartAddr = *((unsigned long *)&FatCache1[COMMON_VOICE_OFFSET/2]);//COMMON_VOICE_OFFSET

	memset(FatCache1, 0, sizeof(FatCache1));
	tmp = fs_seek(pOIDDataFile, 0, HEADMSG_SIZE);
	tmp = fs_read(pOIDDataFile, (u8 *)FatCache1, sizeof(FatCache1));
    //put_buf((u8 *)FatCache1, sizeof(FatCache1));
	if (0 == tmp)
	{
		return 0;
	}

	TotalDotReadNum = *((unsigned long *)&FatCache1[0]);
	FristDotReadOID = *((unsigned long *)&FatCache1[2]);
	MaxDotReadOID = FristDotReadOID + TotalDotReadNum;

	if (GameDataStartAddr != 0)		//取游戏个数及第一个游戏码
	{
		//game data first index
		RdSectNum = GameDataStartAddr / 512;
		Offset = GameDataStartAddr % 512;
		if(!fs_seek(pOIDDataFile, 0, RdSectNum * 512))
		{
			return 0;
		}
		memset(FatCache1, 0, sizeof(FatCache1));
		fs_read(pOIDDataFile, (u8* )FatCache1, sizeof(FatCache1));
		TotalGameCombNum = FatCache1[0];		//游戏组合总数
		GameCombStartOID = FatCache1[1];		//第一个游戏码
	}
    oid_main_printf("fun = %s, line=%d, FristDotReadOID=%x, MaxDotReadOID=%x\n", __func__,__LINE__,FristDotReadOID, MaxDotReadOID);
	return 1;
}


u16 GetDotReadStartPosLenByOID(_FIL_HDL *pOIDDataFile,unsigned long OIDNum,unsigned short Column)
{
	unsigned long ReadDataAddr;
    u16 FatCache1[256];
    int ret =0;
    unsigned long MP3DataStartAddr_tmp=0;
    unsigned long MP3DataLen_tmp = 0;

	ReadDataAddr = ONE_READ_OID_INDEX_CELL * (OIDNum - FristDotReadOID) + DOT_READ_START_POS + DOTREAD_OID_INDEX_TAB_OFFSET;
	memset(FatCache1, 0, sizeof(FatCache1));

	fs_seek(pOIDDataFile, 0, ReadDataAddr);
	fs_read(pOIDDataFile, (u8* )FatCache1, sizeof(FatCache1));

	//点读OID一级索引数据
	DotReadOIDDes.OID_Attr = FatCache1[0];
	DotReadOIDDes.VoiceNumPerOID = FatCache1[1];
	if (DotReadOIDDes.VoiceNumPerOID > 1)
	{
		//整读模式，或者包含多个语音文件，后续完善
		oid_main_printf("fun = %s, line=%d\n", __func__,__LINE__);
	}

	//put_buf((u8* )FatCache1, sizeof(FatCache1));

	DotReadOIDDes.VoiceDataOffset = *((unsigned long *)&FatCache1[2]);

	if(DotReadOIDDes.VoiceDataOffset == 0 || DotReadOIDDes.VoiceNumPerOID == 0)
	{
	    oid_main_printf("fun = %s, line=%d\n", __func__,__LINE__);
		return 0;
	}

	if(Column != 0)
	{
		if(  DotReadOIDDes.VoiceNumPerOID  < Column + 1)
		{
			Column = 0;
		//	return 0;
            oid_main_printf("fun = %s, line=%d\n", __func__,__LINE__);
		}
	}
	else
    {
        oid_main_printf("fun = %s, line=%d, Column=%d\n", __func__,__LINE__,Column);
    }
	//第一个语音起始地址和长度
    memset(FatCache1, 0, sizeof(FatCache1));

    //oid_main_printf("fun = %s, line=%d, DotReadOIDDes.VoiceDataOffset=%08x\n", __func__,__LINE__,DotReadOIDDes.VoiceDataOffset);

	ret = fs_seek(pOIDDataFile, 0, DotReadOIDDes.VoiceDataOffset);
	oid_main_printf("fun = %s, line=%d, ret=%d\n", __func__,__LINE__,ret);
	ret =fs_read(pOIDDataFile, (u8 *)FatCache1, sizeof(FatCache1));
	oid_main_printf("fun = %s, line=%d, ret=%d\n", __func__,__LINE__,ret);

    //put_buf((u8 *)FatCache1, sizeof(FatCache1));

	MP3DataStartAddr_tmp = *((unsigned long *)&FatCache1[Column * 4]);
	MP3DataLen_tmp = *((unsigned long *)&FatCache1[Column * 4 + 2]);

	if ((0 == MP3DataLen_tmp) ||(0 == MP3DataStartAddr_tmp))
	{
        oid_main_printf("fun = %s, line=%d, MP3DataLen_tmp=%x,MP3DataStartAddr_tmp=%x\n", __func__,__LINE__,MP3DataLen_tmp,MP3DataStartAddr_tmp);
		return 0;
	}

    MP3DataStartAddr = MP3DataStartAddr_tmp;
    MP3DataLen = MP3DataLen_tmp;

	return 1;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//Function: GetSystemVoiceStartPosLenByIndex
//Input:	unsigned int SystemVoiceIndex
//Output:	u16 0:play err, 1:play ok
//Description:
//		Play the system voice by index,example:PoweON,PowerOFF, LowBattery
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
u16 GetSystemVoiceStartPosLenByIndex(_FIL_HDL *pOIDDataFile, u16 SystemVoiceIndex)
{
	unsigned long ReadDataAddr;
    u16 FatCache1[256];
    oid_main_printf("fun = %s line=%d\n", __func__,__LINE__);

	MP3DataStartAddr = 0;
	MP3DataLen= 0;
    if(pOIDDataFile==NULL)
    {
        oid_main_printf("fun = %s line=%d\n", __func__,__LINE__);
		return 0;
    }

	ReadDataAddr = ComVoiceDataIndexStartAddr + SystemVoiceIndex * ONE_COM_VOICE_DATA_INDEX;

	fs_seek(pOIDDataFile, 0, ReadDataAddr);
	fs_read(pOIDDataFile, (u8* )FatCache1, sizeof(FatCache1));

	MP3DataStartAddr = *((unsigned long *)&FatCache1[0]);
	MP3DataLen = *((unsigned long *)&FatCache1[2]);

	if ((0 == MP3DataLen) ||(0 == MP3DataStartAddr) || (0xffffffff == MP3DataLen) ||(0xffffffff == MP3DataStartAddr))
	{
		return 0;
	}

	return 1;
}


u16 GetGameCombDesData(_FIL_HDL *pOIDDataFile,unsigned long OIDNum)
{
	unsigned long ReadDataAddr;
	unsigned short i;
	unsigned short LogGameIdxOffset;
    u16 FatCache1[256];

	MP3DataStartAddr = 0;
	MP3DataLen = 0;

	ReadDataAddr = ONE_GAME_OID_FIRST_INDEX_LEN * (OIDNum - GameCombStartOID) + GameDataStartAddr + GAME_FIRST_INDEX_DATA_OFFSET;

	fs_seek(pOIDDataFile, 0, ReadDataAddr);
	fs_read(pOIDDataFile, (u8 *)FatCache1, sizeof(FatCache1));

	GameIdx1.IndexOffset = *((unsigned long *)&FatCache1[0]);
	GameIdx1.IndexLength = FatCache1[2];

	//ReadSectorData(FatCache1,pOIDDataFile,GameIdx1.IndexOffset);
	fs_seek(pOIDDataFile, 0, GameIdx1.IndexOffset);
	fs_read(pOIDDataFile, (u8 *)FatCache1, sizeof(FatCache1));

	GameCombDes.OID_ATTR = *((unsigned long *)&FatCache1[0]);
	if (GameCombDes.OID_ATTR > MAX_GAME_TYPE_NUM)	//不是有效数据
	{
		return -2;
	}

	GameCombDes.GamePromptVoice.offset = *((unsigned long *)&FatCache1[2]);
	GameCombDes.GamePromptVoice.length = *((unsigned long *)&FatCache1[4]);
	GameCombDes.GameNumOneRound = FatCache1[6];
	GameCombDes.TotalGameNum = FatCache1[7];

	if (GameCombDes.OID_ATTR == GAMECOMB_USER_SINGLE)
	{
		for (i = 0;i < GameCombDes.TotalGameNum;i ++)
		{
			GameCombDes.QuestionOIDPool[i] = FatCache1[(GAMECOMB_USER_SINGLE_IDX1_POS + 2 * i ) / 2];
		}
	}
	LogGameIdxOffset = GAMECOMB_USER_SINGLE_IDX1_POS + GameCombDes.TotalGameNum * 2;
	for (i = 0;i < GameCombDes.TotalGameNum;i ++)
	{
		GameCombDes.LogicGameIndexPool[i].IndexOffset = *((unsigned long *)&FatCache1[(LogGameIdxOffset + ONE_GAME_OID_INDEX_CELL * i)/2]);
		GameCombDes.LogicGameIndexPool[i].IndexLength = (unsigned short)*((unsigned long *)&FatCache1[(LogGameIdxOffset + ONE_GAME_OID_INDEX_CELL * i + 4)/2]);
	}


	MP3DataStartAddr = GameCombDes.GamePromptVoice.offset;
	MP3DataLen = GameCombDes.GamePromptVoice.length;

	if ((MP3DataStartAddr == 0) || (MP3DataLen == 0))
	{
		return -1;
	}

	return 0;
}

u16 GetGameCellDesData(_FIL_HDL *pOIDDataFile,unsigned long LogicGameCellNum)
{
	unsigned long ReadDataAddr;
	unsigned short i;
    u16 FatCache1[256];

	MP3DataStartAddr = 0;
	MP3DataLen = 0;

	ReadDataAddr = GameCombDes.LogicGameIndexPool[LogicGameCellNum].IndexOffset;

	fs_seek(pOIDDataFile, 0, ReadDataAddr);
	fs_read(pOIDDataFile, (u8 *)FatCache1, sizeof(FatCache1));

	GameCellDes.OIDAttr = *((unsigned long *)&FatCache1[0]);
	GameCellDes.QuestionVoice.offset = *((unsigned long *)&FatCache1[2]);
	GameCellDes.QuestionVoice.length = *((unsigned long *)&FatCache1[4]);
	GameCellDes.StepPromptNum = FatCache1[6];
	GameCellDes.AnswerNum = FatCache1[7];

	for (i = 0;i < GameCellDes.AnswerNum;i ++)
	{
		GameCellDes.AnswerPool[i] = FatCache1[(ONE_GAMECELL_ANSWERPOOL_OFFSET + i *2)/2];
	}

 	MP3DataStartAddr = GameCellDes.QuestionVoice.offset;
	MP3DataLen = GameCellDes.QuestionVoice.length;
	if ((MP3DataStartAddr == 0) || (MP3DataLen == 0))
	{
		return -1;
	}

	return 0;
}
