#include "task_idle.h"
#include "task_idle_key.h"
#include "msg.h"
#include "task_manager.h"
#include "task_common.h"
#include "audio/dac_api.h"
#include "dac.h"
#include "power_manage_api.h"
#include "dev_manage.h"
#include "warning_tone.h"
#include "led.h"
#include "file_operate.h"
#include "fat_io.h"
#include "power.h"

#ifdef SUPPORT_MS_EXTENSIONS
#pragma bss_seg(	".system_bss")
#pragma data_seg(	".system_data")
#pragma const_seg(	".system_const")
#pragma code_seg(	".system_code")
#endif

#define TASK_IDLE_DEBUG_ENABLE

#ifdef TASK_IDLE_DEBUG_ENABLE
#define task_idle_printf log_printf
#else
#define task_idle_printf(...)
#endif// TASK_IDLE_DEBUG_ENABLE

extern u32 os_time_get(void);
extern u8 usb_slave_is_online(void);
extern u8 get_battery_charge_flag(void);
extern void delay(u32 d);

u8 poweroff_charge = 0;


static tbool task_idle_skip_check(void **priv)
{
    task_idle_printf("task_idle_skip_check !!\n");
    int error = MSG_NO_ERROR;
    int msg = NO_MSG;

    if(*priv ==IDLE_POWER_UP)
    {
        while (1)
        {
            error = task_get_msg(0, 1, &msg);

            if (os_time_get() <300)//3S
            {
                if (task_common_msg_deal(*priv, msg) == false) //*priv 为了PC接入后开机时不立即跳转。  检测SD存在。
                    break;
            }
            else
                break;
        }
    }

    return true;
}


static void *task_idle_init(void *priv)
{
    /* dac_channel_on(MUSIC_CHANNEL, 0); */
    task_idle_printf("task_idle_init !!\n");

    if (priv == IDLE_POWER_OFF)
    {
        tone_play(TONE_POWER_OFF, 0);
    }
    else if (priv == IDLE_POWER_UP)
    {
        fat_init();
        fat_mem_init(NULL, 0);  //使用默认的RAM， overlay task


        if(get_battery_charge_flag())
        {
            led_fre_set(C_RLED_ON_MODE);
            task_post_msg(NULL, 1, SYS_EVENT_PLAY_SEL_END);
        }
        else if(dev_get_fisrt(MUSIC_DEV_TYPE, DEV_ONLINE) == NULL)//开机TF卡有误
        {
            led_fre_set(C_BLED_ON_MODE);
            tone_play(TONE_TF_ERROR, 0);
        }
        else
        {
            led_fre_set(C_BLED_ON_MODE);
            tone_play(TONE_POWER_ON, 0);
        }
    }
    else if(priv == IDLE_SD_ERROR)
    {
        tone_play(TONE_TF_ERROR, 0);
    }
    else if(priv == IDLE_POWER_OFF_NOVOICE)
    {
        SPEAKER_DECODER_LOW();
        led_fre_set(C_BLED_OFF_MODE);
        if(get_battery_charge_flag() == 0)
        {
            POWER_KEEP_OFF();
            enter_sys_soft_poweroff();
        }
        else
        {
            poweroff_charge = 1;
        }
    }
    else
    {
        task_idle_printf("*priv:%x !!\n", priv);
    }

    return priv;
}

static void task_idle_exit(void **hdl)
{
    task_idle_printf("task_idle_exit !!\n");
    task_clear_all_message();
    poweroff_charge = 0;
}


static void task_idle_deal(void *hdl)
{
    int error = MSG_NO_ERROR;
    int msg = NO_MSG;
    task_idle_printf("task_idle_deal !!\n");
    u32 i = 0;

    while (1)
    {

        error = task_get_msg(0, 1, &msg);

        if(poweroff_charge == 0)
        {
            if (task_common_msg_deal(hdl, msg) == false)
            {
                return;
            }
        }

        if (NO_MSG == msg)
        {

            if(poweroff_charge)
            {
                if(get_battery_charge_flag() == 0)
                {
                    task_post_msg(NULL, 1, MSG_POWER_OFF_LONG_DOWN);
                }
            }

            continue;
        }

        task_idle_printf("idle msg = %x\n", msg);
        switch (msg)
        {
        case MSG_HALF_SECOND:
           //  task_idle_printf("-I_H-");
            break;

        ///test ------------------------
        case SYS_EVENT_DEC_END:
            /* task_idle_printf("\n---------------sbc notice SYS_EVENT_DEC_END\n"); */
            /* sbc_notice_stop(sbc_hdl); */
            /* sbc_notice_play(i); */
            /* i++; */
            break;

        case SYS_EVENT_PLAY_SEL_END:
            task_idle_printf("SYS_EVENT_PLAY_TONE_END, tone_var.idx:%d, TONE_SD_ERROR:%d, charge_flag:%d\n",tone_var.idx, TONE_TF_ERROR , get_battery_charge_flag());

            if((hdl == IDLE_POWER_OFF) || (tone_var.idx == TONE_TF_ERROR)||(tone_var.idx == TONE_POWER_OFF)) //关机和卡错误
            {
                task_idle_printf("idle enter soft_poweroff\n");
                #if RTC_CLK_EN
                set_lowpower_keep_32K_osc_flag(1);
                #endif
                led_fre_set(C_BLED_OFF_MODE);

                SPEAKER_DECODER_LOW();
                if(get_battery_charge_flag() ==0)
                {
                    POWER_KEEP_OFF();
                    enter_sys_soft_poweroff();
                }
                else
                {
                    poweroff_charge = 1;
                }

            }
            else if(hdl == IDLE_POWER_UP)
            {
                if(get_battery_charge_flag())
                {
                    poweroff_charge =  1;

                    if(usb_slave_is_online())//进入PC模式不播放提示音
                    {
                        if(task_switch(TASK_ID_PC, IDLE_POWER_UP)==true)
                            return;
                    }
                }
                else
                {
                    if(task_switch(TASK_ID_OID, (void*)TASK_ID_OID)==true)
                        return;
                }
            }
            break;
        case MSG_SD0_OFFLINE:
            SPEAKER_DECODER_LOW();
            //if(get_battery_charge_flag() ==0)
            {
                POWER_KEEP_OFF();
                enter_sys_soft_poweroff();
            }
            break;

        case MSG_PC_ONLINE:
                if(usb_slave_is_online())//进入PC模式不播放提示音
                {
                    if(task_switch(TASK_ID_PC, NULL)==true)
                        return;
                }
        break;
        case MSG_POWER_OFF_LONG_DOWN:
            task_idle_printf("idle power off\n");
            #if RTC_CLK_EN
            set_lowpower_keep_32K_osc_flag(1);
            #endif

            SPEAKER_DECODER_LOW();
            POWER_KEEP_OFF();
            task_idle_printf("idle power off\n");
            enter_sys_soft_poweroff();

            break;

        case MSG_ONE_SECOND:
            task_idle_printf("idle time: %d S\n", os_time_get());
            break;
        default:

            break;
        }
    }
}

const TASK_APP task_idle_info =
{
    /* .name 		= TASK_APP_IDLE, */
    .skip_check = task_idle_skip_check,
    .init 		= task_idle_init,
    .exit 		= task_idle_exit,
    .task 		= task_idle_deal,
    .key 		= &task_idle_key,
};

