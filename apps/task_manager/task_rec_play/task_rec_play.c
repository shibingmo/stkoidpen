#include "msg.h"
#include "uart.h"
#include "clock.h"
#include "audio/audio.h"
#include "music_player.h"
#include "task_common.h"
#include "warning_tone.h"
#include "rec_api.h"
#include "task_rec_play.h"
#include "task_rec_play_key.h"
#include "rec_api.h"
#include "string.h"
#include "wdt.h"
#include "audio/dac_api.h"
#include "ui/ui_api.h"
#include "fat_io.h"
#include "rec_ui.h"
#include "rec_play.h"
#include "dac.h"
#include "led.h"
#include "rec_file_op.h"
#include "task_oid.h"

#define REC_PLAY_TASK_DEBUG_ENABLE
#ifdef REC_PLAY_TASK_DEBUG_ENABLE
#define rec_play_task_printf log_printf
#else
#define rec_play_task_printf(...)
#endif

#if MIC_REC_EN

static MUSIC_DECODER_ST music_status2;
static DEV_HANDLE cur_use_dev2= NULL;
char max_rec_file_path[50] = {0};
extern REC_FILE_INFO play_rec_file;

static void recordfile_play_mutex_init(void *priv)
{
    rec_play_task_printf("%s in...\n", __func__);

    tbool ret;
    MUSIC_PLAYER *obj = priv;
    //  ret = music_player_play_spec_dev(obj, (u32)cur_use_dev2);
    //  ret = music_player_play_path_dev(obj, (u32)cur_use_dev2, (u8*)"/XDY_REC/" ,1 ,PLAY_LAST_FILE);

      rec_play_task_printf("fun = %s, play_path = %s\n", __func__, max_rec_file_path);
      ret = music_player_play_path_file(obj, (u8* )get_rec_filename(play_rec_file.rec_fname_cnt), 0);
    if (ret == false) {
        rec_play_task_printf("fun = %s, line = %d\n", __func__, __LINE__);
        task_post_msg(NULL, 1, MSG_CHANGE_WORKMODE);
    }

}

static void recordfile_play_mutex_stop(void *priv)

{
    rec_play_task_printf("%s in..., priv:0x%x...\n", __func__, priv);

    MUSIC_PLAYER *obj = priv;
    music_status2 = music_player_get_status(obj);
    cur_use_dev2 = music_player_get_cur_dev(obj);

    if (obj)
    {
        music_player_destroy(&obj);
    }
    rec_play_task_printf("%s in..., priv:0x%x...\n", __func__, priv);
}

static MUSIC_PLAYER *recordfile_play_start(void)
{
    rec_play_task_printf("%s in...\n", __func__);

    if(is_cur_resource("recordfile"))
    {
        mutex_resource_release("tone");
        mutex_resource_release("recordfile");
    }


    MUSIC_PLAYER *obj = NULL;
    obj = music_player_creat();
    music_status2 = MUSIC_DECODER_ST_PLAY;

    rec_play_task_printf("music_player_creat:0x%x\n", obj);
    if (obj)
    {
        mutex_resource_apply("recordfile", 3, recordfile_play_mutex_init, recordfile_play_mutex_stop, obj);
    }
    return obj;
}



static tbool task_rec_play_check(void **priv)
{
    rec_play_task_printf("task_rec_play_check !!\n");

    //DEV_HANDLE *dev = *priv;
    u32 parm;
    u32 dev_status;
    tbool dev_cnt = 0;

    //check some device online
    if (*priv == NULL)
    {
        *priv = dev_get_fisrt(MUSIC_DEV_TYPE, DEV_ONLINE);
    }


    // printf("2music cur_use_dev:0x%x,0x%x\n", cur_use_dev, *priv);
    if (*priv == (void *)sd0)
    {
        puts("[PRIV SD0]\n");
    }
    else if (*priv == (void *)sd1)
    {
        puts("[PRIV SD1]\n");
    }
    else if (*priv == (void *)usb)
    {
        puts("[PRIV USB]\n");
    }
    else
    {
        puts("[PRIV CACHE]\n");
    }


    if (*priv == NULL)
    {
        return false;
    }
    else
    {
        cur_use_dev2 = *priv;

    }

    //check specific device online
    return true;
}




static void *task_rec_play_init(void *priv)
{
    rec_play_task_printf("task rec play init !!\n");

    if(set_readyrecnum_by_checklastname() <= 1)
    {
        task_switch(TASK_ID_TYPE_NEXT, NULL);
        return NULL;
    }


    dac_channel_off(LINEIN_CHANNEL, 0);
    dac_channel_on(MUSIC_CHANNEL, 0);
    set_sys_vol(sound.vol.sys_vol_l, sound.vol.sys_vol_r, FADE_ON);
    audio_set_output_buf_limit(OUTPUT_BUF_LIMIT_MUSIC_TASK);

    tone_play(TONE_REC_PLAY_MODE, 0);

    led_fre_set(C_RLED_SLOW_MODE);

    return NULL;
}

static void task_rec_play_exit(void **hdl)
{
    task_clear_all_message();
    mutex_resource_release("recordfile");
    audio_set_output_buf_limit(OUTPUT_BUF_LIMIT);
    set_sys_freq(SYS_Hz);
    task_clear_all_message();

    rec_play_task_printf("task_rec_play_exit !!\n");
}


static void task_rec_play_deal(void *p)
{
    int msg;
    int msg_error = MSG_NO_ERROR;
    tbool ret = true;
    MUSIC_PLAYER *obj = (MUSIC_PLAYER *)p;

    printf("****************REC PLAY TSAK*********************\n");

    while (1)
    {

        msg_error = task_get_msg(0, 1, &msg);

        if ((task_common_msg_deal(NULL, msg) == false)||(turnto_oidmode_check()== true))
        {
            music_tone_stop();
            task_common_msg_deal(NULL, NO_MSG);
            return;
        }

        if (NO_MSG == msg)
        {
            continue;
        }
        else
            rec_play_task_printf("msg:0x%x\n", msg);


        switch (msg)
        {
        case SYS_EVENT_PLAY_SEL_END: //提示音结束
            rec_play_task_printf("SYS_EVENT_PLAY_SEL_END\n");

            obj = recordfile_play_start();

            break;
        case SYS_EVENT_DEC_FR_END:
        case SYS_EVENT_DEC_FF_END:
        case SYS_EVENT_DEC_DEVICE_ERR:
            puts("SYS_EVENT_DEC_DEVICE_ERR\n");
        case SYS_EVENT_DEC_END:
            rec_play_task_printf("SYS_EVENT_DEC_END, fun = %s, line = %d\n", __func__, __LINE__);
            music_player_operation(obj, PLAY_AUTO_NEXT);
            break;

        case MSG_MUSIC_NEXT_FILE:

            music_player_operation(obj, PLAY_NEXT_FILE);

            break;
        case MSG_MUSIC_PREV_FILE:

            music_player_operation(obj, PLAY_PREV_FILE);

            break;



        default:
            break;
        }
    }
}

const TASK_APP task_rec_play_info =
{
    .skip_check = task_rec_play_check,
    .init 		= task_rec_play_init,
    .exit 		= task_rec_play_exit,
    .task 		= task_rec_play_deal,
    .key 		= &task_rec_play_key,
};


#if BT_EMITTER_EN
void *emitter_recordfile_play(void *dev)
{
    static DEV_HANDLE cur_use_dev = NULL;
    MUSIC_PLAYER *obj = NULL;
    tbool ret = 0;

    init_lastandplay_recfile();

    obj = music_player_creat();

    cur_use_dev = dev;

    if ((obj == NULL) || (cur_use_dev == NULL))
    {
        printf("emitter_recordfile_play err0:%x,%x\n", obj, cur_use_dev);
        return NULL;
    }

    file_operate_set_repeat_mode(obj->fop, REPEAT_FOLDER);

    return obj;
}



#endif // BT_EMITTER_EN


#endif
