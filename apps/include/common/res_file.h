#ifndef _RES_FILE_H_
#define	_RES_FILE_H_

#define RES_BT_CFG             	"bt_cfg.bin"
#define RES_HW_EQ_CFG			"eq_cfg.bin"
#define RES_SW_EQ_CFG         	"eq_cfg.bin"



#define RES_POWER_ON_MP3        "/system/pwr_on.***"
#define RES_POWER_OFF_MP3       "/system/pwr_off.***"

#define RES_BT_MP3              "/system/bt_mode.***"
#define RES_CONNECT_MP3         "/system/bt_conn.***"
#define RES_DISCONNECT_MP3      "/system/bt_disc.***"
#define RES_PARING_MP3          "/system/bt_pair.***"

#define RES_OID_MP3             "/system/oid.***"
#define RES_RDFBOOK_MP3         "/system/rf_book.***"
#define RES_CANTRD_MP3          "/system/cantrd.***"
#define RES_READ_FAIL_MP3       "/system/rd_fail.***"
#define RES_OID_NOFILE_MP3      "/system/NoBook.***"

#define RES_MUSIC_MP3           "/system/music.***"
#define RES_MUSIC_NOFILE_MP3    "/system/NoMp3.***"
#define RES_DELETE_FILE_MP3     "/system/delete.***"



#define RES_REC_MP3             "/system/recmode.***"
#define RES_REC_PLAY_MP3        "/system/recplay.***"
#define RES_REC_START_MP3       "/system/recstart.***"
#define RES_REC_END_MP3         "/system/recstop.***"
#define RES_REC_NOFILE_MP3      "/system/NORecFil.***"

#define RES_DING_MP3            "/system/ding.***"
#define RES_DING2_MP3            "/system/ding_2.***"
#define RES_DING3_MP3            "/system/ding_3.***"

#define RES_PC_MP3              "/pc.***"


#define RES_RADIO_MP3           "/radio.***"


#define RES_LINEIN_MP3          "/linein.***"


#define RES_RTC_MP3             "/rtc.***"


#define RES_ECHO_MP3            "/echo.***"








#define RES_RING_MP3            "/ring.***"
#define RES_0_MP3               "/0.***"
#define RES_1_MP3               "/1.***"
#define RES_2_MP3               "/2.***"
#define RES_3_MP3               "/3.***"
#define RES_4_MP3               "/4.***"
#define RES_5_MP3               "/5.***"
#define RES_6_MP3               "/6.***"
#define RES_7_MP3               "/7.***"
#define RES_8_MP3               "/8.***"
#define RES_9_MP3               "/9.***"

#define RES_VOL_UP_MP3          "/system/vol_up.***"
#define RES_VOL_DOWN_MP3        "/system/vol_dn.***"
#define RES_VOL_MAX_MP3         "/system/vol_max.***"
#define RES_VOL_MIN_MP3         "/system/vol_min.***"


#define RES_TF_ERROR_MP3        "/tf_error.***"


#define RES_LOW_POWER_MP3       "/system/low_pwr.***"
#define RES_FULL_BATTERY_MP3    "/system/full.***"
#define RES_EIGHTY_BATTERY_MP3  "/system/eighty.***"
#define RES_SIXTY_BATTERY_MP3   "/system/sixty.***"
#define RES_FORTY_BATTERY_MP3   "/system/forty.***"

#define RES_CHARGING_MP3        "/system/charging.***"


#define RES_AREYOUHERE_MP3      "/system/ayhere.***"
#define RES_WARNING_MP3     	"/warning.***"
#define RES_TEST_MP3       		"/test.***"


#endif
