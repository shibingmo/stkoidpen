#ifndef _EMITTER_MAIN_H_
#define _EMITTER_MAIN_H_

#include "typedef.h"

s32 media_sbc_init();
s32 media_sbc_encode();
s32 send_media_to_sbc_encoder(void *pbuf, u32 len, u16 rate, u8 ch);
u8 emitter_stream_state(void);

extern u32 get_encoder_outbuf_data_len();
extern int get_emitter_encoder_sample_rate();

#endif
