#ifndef _EMITTER_USER_H_
#define _EMITTER_USER_H_

#include "dev_manage.h"
#include "sdk_cfg.h"
#include "emitter_main.h"
#include "task_manager.h"
#define EMITTER_VOL_SYNC			0
/*连接成功自动播放*/
#define EMITTER_AUTO_PLAY			1

/*搜索超时是否继续搜索*/
#define EMITTER_INQUIRY_AGAIN		1

/*是否支持spp链路连接*/
#define EMITTER_SUPPORT_SPP			0

/*使用蓝牙配置里面的名字列表作为名字过滤列表，测试使用*/
#define EMITTER_NAME_FILT_EXT		0

/*pincode键盘输入*/
#define EMITTER_PINCODE_KEYBOARD	0

/*发射器角色定义：发射器/接收器*/
#define EMITTER_ROLE_SLAVE			0
#define EMITTER_ROLE_MASTER			1

/*发射源定义*/
#define EMITTER_SOURCE_NULL      	0
#define EMITTER_SOURCE_AUX       	BIT(0)	/*local aux adc result*/
#define EMITTER_SOURCE_DEC       	BIT(1)	/*local decoder output*/
#define EMITTER_SOURCE_PC        	BIT(2)	/*rx form remote device*/
#define EMITTER_SOURCE_TEST      	BIT(7)	/*define for debug*/

/*搜索结果处理模式定义*/
#define SEARCH_NULL_LIMITED			0x00/*搜索结果不处理,直接连接*/
#define SEARCH_BD_NAME_LIMITED		0x01/*搜索结果按照名字匹配*/
#define SEARCH_BD_ADDR_LIMITED		0x02/*搜索结果按照地址匹配*/
#define SEARCH_CUSTOM_LIMITED		0x03/*搜索结果自定义处理*/
#define SEARCH_LIMITED_MODE			SEARCH_CUSTOM_LIMITED

typedef struct _EMITTER_INFO
{
    volatile u8 role;
    u8 task_id;
    u8 media_source;
    u8 source_record;/*统计当前有多少设备可用*/
    DEV_HANDLE emitter_dev;
    void *dec_obj;
} EMITTER_INFO_T;
extern EMITTER_INFO_T emitter_info;

DEV_HANDLE get_emitter_dev();
void set_emitter_role(u8 role);
u8 get_emitter_role(void);
u8 is_emitter_role_master(void);
void emitter_init(u8 role);
void emitter_start(void);
void emitter_stop(void);
void start_emitter_task_id(TASK_ID_TYPE id);
void emitter_media_source_next();
void emitter_media_source(u8 source, u8 en);
u8 get_emitter_media_source(void);
u8 emitter_msg_deal(int msg);
void set_emitter_task_id(TASK_ID_TYPE id);

TASK_ID_TYPE get_emitter_task_id(void);
//extern api from lib
extern void emitter_info_init(u8 role, u8 retry_counts);

extern u8 a2dp_send_media_to_sbc_encoder(u32 *pbuf, u32 len, u16 rate, u8 ch) ; //ADC 中断调用
extern void __emitter_send_media_toggle(u8 toggle);
extern void search_bd_name_filt_handle_register(u8(*handle)(u8 *data, u8 len)) ;
extern void search_bd_addr_filt_handle_register(u8(*handle)(u8 *addr)) ;
extern void bd_inquiry_result_custom_register(u8(handle)(char *bd_name, u8 name_len, u8 *addr));
u8 emitter_search_result(char *name, u8 name_len, u8 *addr, u32 dev_class, char rssi);
void emitter_rx_avctp_opid_deal(u8 cmd, u8 id);
void emitter_rx_vol_change(u8 vol);
u8 is_emitter_conn_info_exist();
u8 emitter_handle_collect_data(void);
void emitter_start_connect_search_device(u8 index);
#endif
