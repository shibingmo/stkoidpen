#ifndef _CHARGE_H_
#define _CHARGE_H_

#include "typedef.h"
#include "common/common.h"

enum
{
    POWER_ON = 1,
    POWER_OFF,
};

//可配置选项
//充电时间过长，可以调整关机的充电阀值POWEROFF_THRESHOLD_VALUE和开机充电阀值POWERON_THRESHOLD_VALUE宏，值越大，充电时间越短，充满电压越低
#define POWEROFF_THRESHOLD_VALUE        500L
#define POWERON_THRESHOLD_VALUE         580L

#define C_POWER_BAT_CHECK_CNT         50
#define C_POWER_KEY_CHECK_CNT         300



#define CHG_IN_PORT			JL_PORTA
#define CHG_IN_PIN             13


#define CHG_FINISH_PORT     JL_PORTC
#define CHG_FINISH_PIN         5

#define charge_io_init()    do{CHG_IN_PORT->DIR |=  (BIT(CHG_IN_PIN));CHG_IN_PORT->PU |=  (BIT(CHG_IN_PIN));CHG_IN_PORT->PD  &=  ~(BIT(CHG_IN_PIN));\
                                CHG_FINISH_PORT->DIR |=  (BIT(CHG_FINISH_PIN));CHG_FINISH_PORT->PU |=  (BIT(CHG_FINISH_PIN));CHG_FINISH_PORT->PD  &=  ~(BIT(CHG_FINISH_PIN));}while(0)
#define CHG_IN_STATE()     (CHG_IN_PORT->IN&BIT(CHG_IN_PIN))
#define CHG_FINISH_STATE()  (CHG_FINISH_PORT->IN&BIT(CHG_FINISH_PIN))


#define CHG_LED             BIT(6)
#define charge_led_init()   //do{JL_PORTB->DIR &= ~CHG_LED;JL_PORTB->PU &= ~CHG_LED;JL_PORTB->PD &= ~CHG_LED;}while(0)
#define charge_led_on()     //do{JL_PORTB->OUT |=  CHG_LED;}while(0)
#define charge_led_off()    // do{JL_PORTB->OUT &= ~CHG_LED;}while(0)

#endif    //_CHARGE_H_
