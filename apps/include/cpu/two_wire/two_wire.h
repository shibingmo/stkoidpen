#ifndef __TWO_WIRE_H__
#define __TWO_WIRE_H__
#include "sdk_cfg.h"

#if OID_PEN_EN
#define TW_PORT			JL_PORTA

#define TW_SCK				2
#define TW_SDIO				3

#define LOW                 0
#define HIGH                1

void master_setup_slave_init();
u32 get_oidcode(void);
u8 get_readflag(void);
void set_receive_oidcode_enable(void);
void set_receive_oidcode_disable(void);
#endif // OID_PEN_EN

#endif // __TWO_WIRE_H__
