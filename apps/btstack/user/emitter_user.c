#include "bluetooth/avctp_user.h"
#include "common/app_cfg.h"
#include "common/msg.h"
#include "sdk_cfg.h"
#include "clock.h"
#include "rtc/rtc_api.h"
#include "uart_param.h"
#include "emitter_user.h"
#include "audio/ladc.h"
#include "music_player.h"
#include "updata.h"
#include "dev_pc.h"
#include "audio/dac_api.h"
#include "audio/dac.h"
#include "digital_vol.h"
#include "task_manager.h"
#include "fs_io.h"
#include "fat_io.h"
#include "file_op_err.h"
#include "warning_tone.h"
#include "audio.h"
#include "led.h"
#include "task_oid.h"
#include "res_file.h"
#include "oid_main_function.h"
#include "rec_file_op.h"
#include "rec_api.h"
#include "common.h"

#if BT_EMITTER_EN
void sin_tone_play(u16 cnt);
extern void *emitter_recordfile_play(void *dev);
extern void *emitter_oidmp3_mode_start(void *dev);
void oid_play_file_voice(MUSIC_PLAYER *obj, u32 filenum);
extern void clear_oid_open_bkinfo(void);
extern TASK_ID_TYPE back_to_task;

extern REC_FILE_INFO play_rec_file;

EMITTER_INFO_T emitter_info =
{
    .role = EMITTER_ROLE_SLAVE,
};

AT_RAM
u8 get_emitter_role(void)
{
    return emitter_info.role;
}

void set_emitter_role(u8 role)
{
    memset((u8 *)&emitter_info, 0x0, sizeof(emitter_info));
    emitter_info.role = role;
}



DEV_HANDLE get_emitter_dev(void)
{
    return emitter_info.emitter_dev;
}

void set_emitter_dev(DEV_HANDLE dev)
{
    emitter_info.emitter_dev = dev;
}

void set_emitter_task_id(TASK_ID_TYPE id)
{
    printf("set_emitter_task_id :%d\n", id);
    emitter_info.task_id = id;
    if(id != TASK_ID_BT)
        back_to_task = id;
}


TASK_ID_TYPE get_emitter_task_id(void)
{
    //printf("get_emitter_task_id :%d\n", emitter_info.task_id);
    return 	emitter_info.task_id;
}




/*
 *获取当前蓝牙角色：
 *return 1:蓝牙发射器
 *return 0:蓝牙接收器
 */
AT_RAM
u8 is_emitter_role_master(void)
{
    return (emitter_info.role == EMITTER_ROLE_MASTER);
}


u8 emitter_tone_as_musicplay = 0;
//u32 power_off_filenum = 0;
u8 delete_file_flag = 0 ;
u8 emitter_poweroff_flag = 0;
u8 emitter_msg_deal(int msg)
{
    u8 res = 0;
    tbool ret = true;

    if (emitter_info.media_source == EMITTER_SOURCE_DEC)
    {

        if (emitter_info.dec_obj == NULL)
        {
            return 0;
        }
    }

    switch (msg)
    {
    case MSG_OID_READ_FAIL:
        clear_oid_open_bkinfo();
        emitter_tone_as_musicplay = 1;
        if(music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_READ_FAIL_MP3, 0))
        {

        }

        ret = 1;
        break;
    case MSG_LOW_POWER_VOICE:
        clear_oid_open_bkinfo();
        emitter_tone_as_musicplay = 1;
        if(music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_LOW_POWER_MP3, 0))
        {

        }
        ret = 1;
        break;
    case MSG_FULL_POWER:
        clear_oid_open_bkinfo();
        emitter_tone_as_musicplay = 1;
        if(music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_FULL_BATTERY_MP3, 0))
        {

        }

        ret = 1;
        break;
    case MSG_EIGHTY_POWER:
        clear_oid_open_bkinfo();
        emitter_tone_as_musicplay = 1;
        if(music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_EIGHTY_BATTERY_MP3, 0))
        {

        }
        ret = 1;
        break;
    case MSG_SIXTY_POWER:
        clear_oid_open_bkinfo();
        emitter_tone_as_musicplay = 1;
        if(music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_SIXTY_BATTERY_MP3, 0))
        {

        }
        ret = 1;
        break;
    case MSG_FORTY_POWER:
        clear_oid_open_bkinfo();
        emitter_tone_as_musicplay = 1;
        if(music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_FORTY_BATTERY_MP3, 0))
        {

        }
        ret = 1;
        break;
    case MSG_PLAY_SIN_TONE:
        if(music_player_get_status(((MUSIC_PLAYER *)emitter_info.dec_obj))!= MUSIC_DECODER_ST_PLAY)
        {
            //sin_tone_play(100);
        }

        ret = 1;
        break;


    case MSG_LOW_POWER:
    case MSG_POWER_OFF_AUTO:
    case MSG_EMITTER_PLAY_POWEROFF:
        clear_oid_open_bkinfo();
        emitter_tone_as_musicplay = 1;
        if(music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_POWER_OFF_MP3, 0))
        {
            printf("file_num:%d\n",((MUSIC_PLAYER *)(emitter_info.dec_obj))->fop->fop_info->filenum);
            //power_off_filenum = ((MUSIC_PLAYER *)(emitter_info.dec_obj))->fop->fop_info->filenum;
            emitter_poweroff_flag = 1;
        }

        ret = 1;
        break;


    case SYS_EVENT_PLAY_SEL_END://提示音结束

        break;

    case MSG_MUSIC_PP:
        if ((get_emitter_task_id() == TASK_ID_MUSIC) || (get_emitter_task_id() == TASK_ID_OID) || (get_emitter_task_id() == TASK_ID_REC))
            music_player_pp(emitter_info.dec_obj);

        ret = 1;
        break;
    case  MSG_MUSIC_PLAY:
        #if OID_PEN_EN
        if(get_emitter_task_id()==TASK_ID_OID)
        {
            extern u16 BookIdxInlstFile;
            oid_play_file_voice((MUSIC_PLAYER *)emitter_info.dec_obj, BookIdxInlstFile);
        }

        ret = 1;
        #endif // OID_PEN_EN
        break;

    case MSG_OID_REPEAT_SHORT:


        if(get_emitter_task_id() == TASK_ID_OID)
        {
            Set_RepeatReadMax(0);

            clear_oid_open_bkinfo();
            emitter_tone_as_musicplay = 1;

            if(music_player_get_status(((MUSIC_PLAYER *)emitter_info.dec_obj))== MUSIC_DECODER_ST_PLAY)
            {
                ret = 1;        res = 1;
                break;
            }
            else
            {
                    if(Get_RepeatReadMax() == 1)
                    {
                        ret = music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_DING_MP3, 0);
                    }
                    else if(Get_RepeatReadMax() == 2)
                    {
                        ret = music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_DING2_MP3, 0);
                    }
                    else if(Get_RepeatReadMax() == 3)
                    {
                        ret = music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_DING3_MP3, 0);
                    }
            }





            #if 0
            if(Get_RepeatReadMax() == 1)
            {
                sin_tone_play(100);
            }
            else if(Get_RepeatReadMax() == 2)
            {
                sin_tone_play(100);
                delay_2ms(100);
                sin_tone_play(100);
            }
            else if(Get_RepeatReadMax() == 3)
            {
                sin_tone_play(100);
                delay_2ms(100);
                sin_tone_play(100);
                delay_2ms(100);
                sin_tone_play(100);
            }
            #endif // 0

        }
        res = 1;
        break;

    case MSG_OID_REPEAT_LONG:


        if(get_emitter_task_id() == TASK_ID_OID)
        {
            Set_RepeatReadMax(1);

            clear_oid_open_bkinfo();
            emitter_tone_as_musicplay = 1;

            if(music_player_get_status(((MUSIC_PLAYER *)emitter_info.dec_obj))== MUSIC_DECODER_ST_PLAY)
            {
                ret = 1;        res = 1;
                break;
            }
            else
            if(Get_RepeatReadMax() == 0xFFFF)
            {
                //sin_tone_play(100);
                ret = music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_DING_MP3, 0);
            }
        }
        res = 1;
        break;

    case MSG_MUSIC_DEL_FILE:
        if(music_player_get_status(((MUSIC_PLAYER *)emitter_info.dec_obj))!= MUSIC_DECODER_ST_PLAY)
        {
            ret = 1;
            break;
        }


        if((get_emitter_dev()!=NULL) && ((get_emitter_task_id() == TASK_ID_MUSIC)||(get_emitter_task_id() == TASK_ID_REC)))
           {
                u32 filenum;
                filenum = music_player_get_file_number(emitter_info.dec_obj);
                ret = music_player_delete_playing_file(emitter_info.dec_obj);
                delete_file_flag =1;

                if(get_emitter_task_id() == TASK_ID_MUSIC)
                {
                    player_is_exist_playfile(((MUSIC_PLAYER *)(emitter_info.dec_obj))->fop, (u8 *)"/mp3/");
                }
                else if(get_emitter_task_id() == TASK_ID_REC)
                {

                    is_exist_recfile_bypath(((MUSIC_PLAYER *)(emitter_info.dec_obj))->fop, (u8 *)"/XDY_REC/");


                    if(get_enter_is_exist_recfile() == 1)
                    {
                        rec_fop_update_playfile(&play_rec_file, 1);
                    }
                }

                clear_oid_open_bkinfo();
                emitter_tone_as_musicplay = 1;

                music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_DING_MP3, 0);


                /*
                if(get_emitter_task_id() == TASK_ID_MUSIC)
                {
                    if(get_music_is_exist_playfile()==0)
                    {
                        clear_oid_open_bkinfo();
                        emitter_tone_as_musicplay = 1;
                        ret = music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_MUSIC_NOFILE_MP3, 0);
                    }
                    else
                    {
                        ret = music_player_play_spec_num_file(emitter_info.dec_obj, filenum);
                    }

                }
                else if(get_emitter_task_id() == TASK_ID_REC)
                {
                    if(get_enter_is_exist_recfile() == 1)
                    {
                        rec_fop_update_playfile(&play_rec_file, 1);
                        music_player_play_path_file(emitter_info.dec_obj, (u8 *)get_rec_filename(play_rec_file.rec_fname_cnt), 0);
                    }
                }
                */
           }

         res = 1;
        break;
    case MSG_EMITTER_NEXT_MODE:
        if(get_emitter_dev()!=NULL)
        {
            start_emitter_task_id(TASK_ID_BT);    //next mode
        }
        res = 1;
        break;
    case SYS_EVENT_DEC_DEVICE_ERR:
        puts("SYS_EVENT_DEC_DEVICE_ERR\n");
        //ret = music_player_play_next_dev(emitter_info.dec_obj);
        break;
    case SYS_EVENT_DEC_END: //解码结束
        puts("music_play_end...................\n");

        music_decoder_stop(((MUSIC_PLAYER *)(emitter_info.dec_obj))->dop);

        //if(power_off_filenum == ((MUSIC_PLAYER *)(emitter_info.dec_obj))->fop->fop_info->filenum) //关机
        //{
        //    task_post_msg(NULL, 1, MSG_TURN_TO_IDLE);
        //    res = 1;
        //    emitter_tone_as_musicplay = 0;
        //    break;
        //}
        if(emitter_poweroff_flag == 1)
        {
            emitter_poweroff_flag = 0;
            task_post_msg(NULL, 1, MSG_TURN_TO_IDLE);
            res = 1;
            emitter_tone_as_musicplay = 0;
            break;
        }


        if(get_emitter_task_id() == TASK_ID_OID)
        {
            if(emitter_tone_as_musicplay == 2)
            {
                if(get_BookFileCnt() == 0)
                {
                    emitter_tone_as_musicplay = 1;
                    music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_OID_NOFILE_MP3, 0);
                    break;
                }
            }
            else if(emitter_tone_as_musicplay == 1)
            {

            }
            else if(emitter_tone_as_musicplay == 0)
            {
                if(Get_RepeatReadMax()>1)
                {
                    if(--RepeatReadCnt)
                    {
                        task_post_msg(NULL, 1, MSG_MUSIC_PLAY);
                    }
                }
            }
        }
        else if(get_emitter_task_id() == TASK_ID_MUSIC)
        {
            if(emitter_tone_as_musicplay == 2)
            {
                if(get_music_is_exist_playfile())
                {
                    emitter_tone_as_musicplay = 0;
                    music_player_play_spec_dev((MUSIC_PLAYER *)(emitter_info.dec_obj), (u32)get_emitter_dev());
                }
                else
                {
                    emitter_tone_as_musicplay = 1;
                    music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_MUSIC_NOFILE_MP3, 0);
                    break;
                }
            }
            else if(emitter_tone_as_musicplay == 1)
            {
                if(get_music_is_exist_playfile())
                {
                    delete_file_flag = 0;
                    emitter_tone_as_musicplay = 0;
                    music_player_play_spec_dev((MUSIC_PLAYER *)(emitter_info.dec_obj), (u32)get_emitter_dev());

                }
                else
                {
                    if(delete_file_flag)
                    {
                        delete_file_flag = 0;
                        emitter_tone_as_musicplay = 1;
                        music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_MUSIC_NOFILE_MP3, 0);
                        break;
                    }

                }
            }
            else if(emitter_tone_as_musicplay == 0)
            {
                 ret = music_player_operation(((MUSIC_PLAYER *)(emitter_info.dec_obj)), PLAY_AUTO_NEXT);
            }

        }
		else if(get_emitter_task_id() == TASK_ID_REC)
        {
            if(emitter_tone_as_musicplay == 2)
            {
                if(get_enter_is_exist_recfile() == 1)
                {
                    emitter_tone_as_musicplay = 0;
                    music_player_play_path_file(emitter_info.dec_obj, (u8 *)get_rec_filename(play_rec_file.rec_fname_cnt), 0);
                }
                else
                {
                    emitter_tone_as_musicplay = 1;
                    music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_REC_NOFILE_MP3, 0);
                    break;
                }
            }
            else if(emitter_tone_as_musicplay == 1)
            {
                if(delete_file_flag)
                {
                    delete_file_flag = 0;
                    if(get_enter_is_exist_recfile() == 1)
                    {
                        emitter_tone_as_musicplay = 0;
                        music_player_play_path_file(emitter_info.dec_obj, (u8 *)get_rec_filename(play_rec_file.rec_fname_cnt), 0);
                    }
                    else
                    {
                        emitter_tone_as_musicplay = 1;
                        music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_REC_NOFILE_MP3, 0);
                        break;
                    }
                }

            }
            else if(emitter_tone_as_musicplay == 0)
            {

            }
        }

        emitter_tone_as_musicplay = 0;
        res = 1;
        break;
    case MSG_BT_PP:
        puts("KEY_PP\n");
        if (emitter_info.media_source == EMITTER_SOURCE_DEC)
        {
            music_player_pp(emitter_info.dec_obj);
        }
        else if (emitter_info.media_source == EMITTER_SOURCE_PC)
        {
            app_usb_slave_hid(USB_AUDIO_PP);
        }
        res = 1;
        break;
    case MSG_BT_NEXT_FILE:
        puts("KEY_NEXT\n");
        if (emitter_info.media_source == EMITTER_SOURCE_DEC)
        {
            if(get_emitter_task_id()== TASK_ID_REC)
            {
                if(get_enter_is_exist_recfile() == 0)
                {
                    emitter_tone_as_musicplay = 1;
                    music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_REC_NOFILE_MP3, 0);
                }
                else
                {
                    emitter_tone_as_musicplay = 0;
                    rec_fop_update_playfile(&play_rec_file, 1);
                    music_player_play_path_file(emitter_info.dec_obj, (u8 *)get_rec_filename(play_rec_file.rec_fname_cnt), 0);
                }
            }
            else if(get_emitter_task_id()== TASK_ID_MUSIC)
            {
                if(get_music_is_exist_playfile() == 0)
                {
                    emitter_tone_as_musicplay = 1;
                    music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_MUSIC_NOFILE_MP3, 0);
                }
                else
                {
                    ret = music_player_operation(emitter_info.dec_obj, PLAY_NEXT_FILE);
                }
            }
        }
        else if (emitter_info.media_source == EMITTER_SOURCE_PC)
        {
            app_usb_slave_hid(USB_AUDIO_NEXTFILE);
        }
        res = 1;
        break;
    case MSG_BT_PREV_FILE:
        puts("KEY_PREV\n");
        if (emitter_info.media_source == EMITTER_SOURCE_DEC)
        {
            if(get_emitter_task_id()== TASK_ID_REC)
            {
                if(get_enter_is_exist_recfile() == 0)
                {
                    emitter_tone_as_musicplay = 1;
                    music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_REC_NOFILE_MP3, 0);
                }
                else
                {
                    emitter_tone_as_musicplay = 0;
                    rec_fop_update_playfile(&play_rec_file, 1);
                    music_player_play_path_file(emitter_info.dec_obj, (u8 *)get_rec_filename(play_rec_file.rec_fname_cnt), 0);
                }
            }
            else  if(get_emitter_task_id()== TASK_ID_MUSIC)
            {
                if(get_music_is_exist_playfile() == 0)
                {
                    emitter_tone_as_musicplay = 1;
                    music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_MUSIC_NOFILE_MP3, 0);
                }
                else
                {
                    ret = music_player_operation(emitter_info.dec_obj, PLAY_PREV_FILE);
                }
            }
        }
        else if (emitter_info.media_source == EMITTER_SOURCE_PC)
        {
            app_usb_slave_hid(USB_AUDIO_PREFILE);
        }
        res = 1;
        break;
    case MSG_UPDATA:
        puts("MSG_UPDATA\n");
        device_updata(music_player_get_cur_dev(emitter_info.dec_obj));
        res = 1;
        break;
    }

    if (ret == false)
    {
        if (emitter_info.media_source == EMITTER_SOURCE_DEC)
        {
            puts("music_operation err\n");
            emitter_media_source(EMITTER_SOURCE_DEC, 0);
            emitter_music_stop(emitter_info.dec_obj);
            emitter_media_source_next();
        }
    }

    return res;
}

typedef enum
{
    AVCTP_OPID_VOLUME_UP   = 0x41,
    AVCTP_OPID_VOLUME_DOWN = 0x42,
    AVCTP_OPID_MUTE        = 0x43,
    AVCTP_OPID_PLAY        = 0x44,
    AVCTP_OPID_STOP        = 0x45,
    AVCTP_OPID_PAUSE       = 0x46,
    AVCTP_OPID_NEXT        = 0x4B,
    AVCTP_OPID_PREV        = 0x4C,
} AVCTP_CMD_TYPE;

/*
 *发射器收到接收器发过来的控制命令处理
 *根据实际需求可以在收到控制命令之后做相应的处理
 */
void emitter_rx_avctp_opid_deal(u8 cmd, u8 id)
{
    printf("avctp_rx_cmd:%x\n", cmd);
    switch (cmd)
    {
    case AVCTP_OPID_NEXT:
        puts("AVCTP_OPID_NEXT\n");
        break;
    case AVCTP_OPID_PREV:
        puts("AVCTP_OPID_PREV\n");
        break;
    case AVCTP_OPID_PAUSE:
    case AVCTP_OPID_PLAY:
    case AVCTP_OPID_STOP:
        puts("AVCTP_OPID_PP\n");
        break;
    case AVCTP_OPID_VOLUME_UP:
        puts("AVCTP_OPID_VOLUME_UP\n");
        break;
    case AVCTP_OPID_VOLUME_DOWN:
        puts("AVCTP_OPID_VOLUME_DOWN\n");
        break;
    default:
        break;
    }
}

void emitter_rx_vol_change(u8 vol)
{
    u8 volume = (vol *  MAX_DIGITAL_VOL_L / 127) ;
    printf("vol_change:%d -> %d\n", vol, volume);
    set_digital_vol(volume, volume);
}

/*当前设备掉线或者出错，或者手动切换发射源，切换下一个发射源*/
void emitter_media_source_next()
{
    if (emitter_info.source_record & EMITTER_SOURCE_AUX)
    {
        puts("emitter source next: aux\n");
        emitter_media_source(EMITTER_SOURCE_AUX, 1);
        emitter_aux_open(NULL);
    }
    else if (emitter_info.source_record & EMITTER_SOURCE_DEC)
    {
        puts("emitter source next: dec\n");
        DEV_HANDLE dev = NULL;
        u32 dev_status;
        if (!dev_get_online_status(usb, &dev_status))
        {
            printf("usb dev_status:%x\n", dev_status);
            if (dev_status == DEV_ONLINE)
            {
                dev = usb;
                goto DEV_CHECK_END;
            }
        }
        if (!dev_get_online_status(sd0, &dev_status))
        {
            printf("sd0 dev_status:%x\n", dev_status);
            if (dev_status == DEV_ONLINE)
            {
                dev = sd0;
                goto DEV_CHECK_END;
            }
        }
        if (!dev_get_online_status(sd1, &dev_status))
        {
            printf("sd1 dev_status:%x\n", dev_status);
            if (dev_status == DEV_ONLINE)
            {
                dev = sd1;
                goto DEV_CHECK_END;
            }
        }
DEV_CHECK_END:
        set_emitter_dev(dev);
        if (dev == NULL)
        {
            puts("dec dev NULL\n");
            return;
        }
        emitter_media_source(EMITTER_SOURCE_DEC, 1);
        emitter_info.dec_obj = emitter_music_play((void *)dev);
        if (emitter_info.dec_obj == NULL)
        {
            emitter_media_source(EMITTER_SOURCE_DEC, 0);
        }
    }
    else if (emitter_info.source_record & EMITTER_SOURCE_PC)
    {
        puts("emitter source next: pc\n");
        if (app_usb_slave_init())
        {
            puts("pc init success\n");
            pc_speak_out_sw(1);
            dac_set_samplerate(SR44100, 0);//usb audio 48k_src_44.1k
            emitter_media_source(EMITTER_SOURCE_PC, 1);
        }
    }
}

s32 emitter_media_source_close(u8 source)
{
    switch (source)
    {
    case EMITTER_SOURCE_DEC:
        puts("close emitter DEC\n");
        emitter_music_stop(emitter_info.dec_obj);
        break;
    case EMITTER_SOURCE_AUX:
        puts("close emitter AUX\n");
        emitter_aux_close(NULL);
        break;
    case EMITTER_SOURCE_PC:
        puts("close emitter PC\n");
        app_usb_slave_close();
        break;
    }
    return 0;
}

void emitter_media_source(u8 source, u8 en)
{
    if (en)
    {
        /*关闭当前的source通道*/
        emitter_media_source_close(emitter_info.media_source);
        emitter_info.source_record |= source;
        if (emitter_info.media_source == source)
        {
            return;
        }
        emitter_info.media_source = source;
        __emitter_send_media_toggle(1);
    }
    else
    {
        emitter_info.source_record &= ~source;
        if (emitter_info.media_source == source)
        {
            emitter_info.media_source = EMITTER_SOURCE_NULL;
            __emitter_send_media_toggle(0);
            emitter_media_source_next();
        }
    }
    printf("current source: %x-%x\n", source, emitter_info.source_record);
}


u8 get_emitter_media_source(void)
{
    return emitter_info.media_source;
}

#if (SEARCH_LIMITED_MODE == SEARCH_BD_ADDR_LIMITED)
u8 bd_addr_filt[][6] =
{
    {0x8E, 0xA7, 0xCA, 0x0A, 0x5E, 0xC8}, /*S10_H*/
    {0xA7, 0xDD, 0x05, 0xDD, 0x1F, 0x00}, /*ST-001*/
    {0xE9, 0x73, 0x13, 0xC0, 0x1F, 0x00}, /*HBS 730*/
    {0x38, 0x7C, 0x78, 0x1C, 0xFC, 0x02}, /*Bluetooth*/
};
u8 search_bd_addr_filt(u8 *addr)
{
    u8 i;
    puts("bd_addr:");
    put_buf(addr, 6);
    for (i = 0; i < (sizeof(bd_addr_filt) / sizeof(bd_addr_filt[0])); i++)
    {
        if (memcmp(addr, bd_addr_filt[i], 6) == 0)
        {
            printf("bd_addr match:%d\n", i);
            return TRUE;
        }
    }
    puts("bd_addr not match\n");
    return FALSE;
}
#endif

#if (SEARCH_LIMITED_MODE == SEARCH_BD_NAME_LIMITED)
#if (EMITTER_NAME_FILT_EXT == 0)
u8 bd_name_filt[10][32];
#if 0
u8 bd_name_filt[][32] =
{
    "FLYING PIGGY",
    "pet singer",
    "HUAWEI CM510",
    "BTW303",
    "X9",
    "EDIFIER CSR8635",/*CSR*/
    "Sabbat X12 Pro",/*Realtek*/
    "I7-TWS",/*ZKLX*/
    "TWS-i7",/*ZKLX*/
    "I9",/*ZKLX*/
    "小米小钢炮蓝牙音箱",/*XiaoMi*/
    "小米蓝牙音箱",/*XiaoMi*/
    "XMFHZ02",/*XiaoMi*/
    "JBL GO 2",
    "i7mini",/*JL tws AC690x*/
    "S08U",
    "S046",/*BK*/
    "AirPods",
    "CSD-TWS-01",
    "AC692x_GZR",
    "JBL Flip 4",
    "BT Speaker",
    "CSC608",
    "QCY-QY19",
    "Newmine",
    "HT1+",
    "S-35",
    "HUAWEI AM08",
    "HUAWEI  FreeBuds",
    "AC69_Bluetooth",
    "BV200",
    "MNS",
    "Jam Heavy Metal",
    "Bluedio",
    "HR-686",
    "BT MUSIC",
    "BW-USB-DONGLE",
    "S530",
    "XPDQ7",
    "MICGEEK Q9S",
    "S10_H",
    "S10",/*JL AC690x*/
    "S11",/*JL AC460x*/
    "HBS-730",
    "SPORT-S9",
    "Q5",
    "IAEB25",
    "QY7",
    "MS-808",
    "LG HBS-730",
    "NG-BT07"
};
#endif // 0
#else
u8 bd_name_filt[20][30] =
{
    "AC69_BT_SDK",
};
#endif
#endif
typedef struct BT_collect_data
{
    char name[62];
    u8 addr[6];
    char rssi[10];
    u8 collect_cnt;
} BT_data;

BT_data BT_search_data[10];





#if (SEARCH_LIMITED_MODE == SEARCH_BD_NAME_LIMITED)
u8 read_bt_voicebox_name_file(void)
{
    u8 f_buf[512]= {'\0'};
    s32 err, f_size, i=0, j=0, k=0;
    FILE_OPERATE *fop_api;
    static u8 first_read=0;

    if(first_read)
        return false;

    memset(bd_name_filt,0,sizeof(bd_name_filt));

    //  fat_init();
    //  fat_mem_init(NULL, 0);

    fop_api = file_operate_creat();

    if (fop_api == NULL)
    {
        printf("creat fop_api err...\n");
        return false;
    }

    file_operate_set_dev(fop_api, (u32)sd0);//only use sd0

    err = file_api_creat(fop_api->fop_file, (void *)fop_api->fop_info->dev, 0);

    if (err == false)
    {
        printf("creat file_api err...\n");
        return false;
    }

    err = fs_get_file_bypath(&(fop_api->fop_file->fs_hdl),&(fop_api->fop_file->file_hdl), (void *)"/bttest.txt");
    printf("err = %d \n", err);
    if(err == FILE_OP_NO_ERR)
    {
        if(fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FILE_SIZE, &f_size))
        {
            printf("read f_size err...\n");
            return false;
        }
        if(f_size > 512)
            f_size = 512;

        fs_read(&(fop_api->fop_file->file_hdl), f_buf, f_size);

        //printf("f_size:%d, f_buf:%s \n", f_size, f_buf);

        for( ; k < f_size; k++)
        {
            //printf("%d %d %d:%c->0x%x\n ",i,j,k,f_buf[k],f_buf[k] );

            if(i<10&&j<32)
            {
                if(f_buf[k] == '\r'&& f_buf[k+1] == '\n'&&(k+1)<f_size)
                {
                    bd_name_filt[i][j]='\0';
                    i++;
                    k++;
                    j = 0;
                }
                else
                {
                    bd_name_filt[i][j++]=f_buf[k];
                }
            }
            else if(j>=32)
            {
                bd_name_filt[i][j-1]='\0';
                j = 0;
            }
        }


        //for(i = 0; i< 10; i++)
        //    printf("%d:%s\n", i, bd_name_filt1[i]);


        fs_close(&(fop_api->fop_file->file_hdl));
    }

//   fat_del();
    first_read = 1;
    return true;
}

u8 search_bd_name_filt(char *data, u8 len, u32 dev_class, char rssi)
{
    char bd_name[62] = {0};
    u8 i;

    if ((len > (sizeof(bd_name))) || (len == 0))
    {
        //printf("bd_name_len error:%d\n", len);
        return FALSE;
    }

    memcpy(bd_name, data, len);
    printf("inquiry_bd_name:%s,len:%d\n", bd_name, len);

    for (i = 0; i < (sizeof(bd_name_filt) / sizeof(bd_name_filt[0])); i++)
    {

        if (memcmp(data, bd_name_filt[i], len) == 0)
        {
            puts("\n*****find dev ok******\n");
            return TRUE;
        }
    }

    return FALSE;
}
#endif


void emitter_collect_search_result(char *bt_name, u8 name_len, u8 *addr, char rssi)
{
    int i = 0;
    for( ; i < 10; i++)
    {
        if( (memcmp(BT_search_data[i].addr, addr, 6)!=0) && (BT_search_data[i].collect_cnt==0) )
        {
            memcpy(BT_search_data[i].name, bt_name, name_len);
            memcpy(BT_search_data[i].addr, addr, 6);
            BT_search_data[i].rssi[BT_search_data[i].collect_cnt] = rssi;
            BT_search_data[i].collect_cnt++;
            break;
        }
        else if(memcmp(BT_search_data[i].addr, addr, 6)==0)
        {
            if(BT_search_data[i].collect_cnt <= 9)
            {
                BT_search_data[i].rssi[BT_search_data[i].collect_cnt] = rssi;
                BT_search_data[i].collect_cnt++;
            }
            break;
        }
    }
}


u8 emitter_handle_collect_data(void)
{
    int i = 0, j = 0, rssi_sum =0;
    int max = 10, Average = -255;

    for( ; i < 10; i++)
    {
        if(BT_search_data[i].collect_cnt)
        {
            printf("%d:\nname:%s ,conllect_cnt=%d",i,BT_search_data[i].name,BT_search_data[i].collect_cnt);
            put_buf(BT_search_data[i].addr, 6);

            rssi_sum = 0;
            for(j = 0 ; j < 10; j++)
            {
                printf("rssi:%d ",BT_search_data[i].rssi[j]);
                rssi_sum += BT_search_data[i].rssi[j];
            }
            printf("average:%d,%d / %d \n ",rssi_sum/BT_search_data[i].collect_cnt, rssi_sum,BT_search_data[i].collect_cnt);
            if((rssi_sum/BT_search_data[i].collect_cnt) > Average)
            {
                max = i;
                Average = rssi_sum/BT_search_data[i].collect_cnt;
            }
        }
    }


    return max;
}

void emitter_start_connect_search_device(u8 index)
{
    extern void set_a2dp_source_addr(u8 *addr);

    if(index < 10)
    {
        set_a2dp_source_addr(BT_search_data[index].addr);
        user_send_cmd_prepare(USER_CTRL_START_CONNEC_VIA_ADDR, 6, BT_search_data[index].addr);

        memset(&BT_search_data[0], 0, sizeof(BT_data));
    }

}


/*
 *inquiry result
 *蓝牙设备搜索结果，可以做名字/地址过滤，也可以保存搜到的所有设备
 *在选择一个进行连接，获取其他你想要的操作。
 *返回TRUE，表示搜到指定的想要的设备，搜索结束，直接连接当前设备
 *返回FALSE，则继续搜索，直到搜索完成或者超时
 */
u8 emitter_search_result(char *name, u8 name_len, u8 *addr, u32 dev_class, char rssi)
{
    printf("emitter_search_result:%s:%d\n", name,rssi);

#if (SEARCH_LIMITED_MODE == SEARCH_BD_NAME_LIMITED)
    return search_bd_name_filt(name, name_len, dev_class, rssi);
#endif

#if (SEARCH_LIMITED_MODE == SEARCH_BD_ADDR_LIMITED)
    return search_bd_addr_filt(addr);
#endif

#if (SEARCH_LIMITED_MODE == SEARCH_CUSTOM_LIMITED)
    /*以下为搜索结果自定义处理*/
    char bt_name[62] = {0};
    u8 len;
    if ( (name_len == 0) || (rssi == 0) )
    {
        puts("No_eir\n");
        return FALSE;
    }
    else
    {
        len = (name_len > 61) ? 61 : name_len;
        /* display bd_name */
        memcpy(bt_name, name, len);
        printf("inquiry_bd_name:%s,len:%d\n", bt_name, name_len);
    }

    /* display bd_addr */
    put_buf(addr, 6);

    emitter_collect_search_result(bt_name,len, addr, rssi);
    /* You can connect the specified bd_addr by below api      */
    //user_send_cmd_prepare(USER_CTRL_START_CONNEC_VIA_ADDR,6,addr);

    return FALSE;
#endif

#if (SEARCH_LIMITED_MODE == SEARCH_NULL_LIMITED)
    /*没有指定限制，则搜到什么就连接什么*/
    return TRUE;
#endif
}

#define SBC_SEND_RETRY_COUNTS	35///<retry counts maximum

void emitter_init(u8 role)
{
    puts("emitter_init\n");
    if (role == EMITTER_ROLE_MASTER)
    {
        emitter_info_init(get_emitter_role(), SBC_SEND_RETRY_COUNTS);
        media_sbc_init();
        memset(&BT_search_data[0], 0, sizeof(BT_data));
        #if (SEARCH_LIMITED_MODE == SEARCH_BD_NAME_LIMITED)
                read_bt_voicebox_name_file();
        #endif
    }
}

extern u8 usb_slave_is_online(void);
extern u8 get_aux_sta(void);

/*轮询设备，将找到的第一个在线设备进行发射*/
void emitter_start(void)
{
    set_emitter_task_id(TASK_ID_BT);
    start_emitter_task_id(back_to_task);
}


void start_emitter_task_id(TASK_ID_TYPE id)
{
    DEV_HANDLE dev = NULL;
    u32 dev_status;
    TASK_ID_TYPE emitter_task_id = id;

    printf("start_emitter_task_id:%d\n", emitter_task_id);
    if(id == TASK_ID_BT)//NEXT
    {
        emitter_task_id = get_emitter_task_id();
        emitter_task_id++;
        printf("emitter_task_id:%d, get_emitter_task_id():%d\n", emitter_task_id, get_emitter_task_id());
    }
    else if(id == get_emitter_task_id())
    {
        printf("id:%d get_emitter_task_id():%d\n", id, get_emitter_task_id());
        return ;
    }



    while(emitter_task_id != get_emitter_task_id())
    {
        printf("1 emitter_task_id:%d, get_emitter_task_id():%d\n", emitter_task_id, get_emitter_task_id());
        switch(emitter_task_id)
        {
        case TASK_ID_MUSIC:
        case TASK_ID_OID:
        case TASK_ID_REC:
            if (!dev_get_online_status(usb, &dev_status))
            {
                printf("usb dev_status:%x\n", dev_status);
                if (dev_status == DEV_ONLINE)
                {
                    dev = usb;
                    goto DEV_CHECK_END;
                }
            }

            if (!dev_get_online_status(sd0, &dev_status))
            {
                printf("sd0 dev_status:%x\n", dev_status);
                if (dev_status == DEV_ONLINE)
                {
                    dev = sd0;
                    goto DEV_CHECK_END;
                }
            }
            else
                printf("sd0_status:%x\n", dev_status);

            if (!dev_get_online_status(sd1, &dev_status))
            {
                printf("sd1 dev_status:%x\n", dev_status);
                if (dev_status == DEV_ONLINE)
                {
                    dev = sd1;
                    goto DEV_CHECK_END;
                }
            }
DEV_CHECK_END:

            if (dev)
            {
                puts("emitter start : DEC\n");

                set_emitter_dev(dev);
                emitter_media_source(EMITTER_SOURCE_DEC, 1);
                emitter_tone_as_musicplay = 0;

                if(emitter_task_id == TASK_ID_OID)
                {
                    led_fre_set(C_BLED_ON_MODE);
                    emitter_info.dec_obj = emitter_oidmp3_mode_start((void*)dev);
                    if(emitter_info.dec_obj)
                    {
                        printf("back_to_task:%d emitter_task_id:%d\n", back_to_task,emitter_task_id);

                        if(back_to_task != emitter_task_id)
                        {
                            clear_oid_open_bkinfo();
                            emitter_tone_as_musicplay = 2;

                            set_emitter_task_id(emitter_task_id);//set before play

                            music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_OID_MP3, 0);
                        }
                        else
                        {
                            set_emitter_task_id(emitter_task_id);
                        }
                    }
                }
                else if(emitter_task_id == TASK_ID_MUSIC)
                {
                    led_fre_set(C_BLED_ON_MODE);
                    emitter_info.dec_obj = emitter_music_play((void*)dev);

                    if(emitter_info.dec_obj)
                    {
                        if(back_to_task != emitter_task_id)
                        {
                            clear_oid_open_bkinfo();
                            emitter_tone_as_musicplay = 2;
                            set_emitter_task_id(emitter_task_id);

                            music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_MUSIC_MP3, 0);

                            file_operate_set_repeat_mode(((MUSIC_PLAYER*)(emitter_info.dec_obj))->fop, REPEAT_FOLDER);
                        }
                        #if 1
                        else
                        {
                            set_emitter_task_id(emitter_task_id);

                            if(get_music_is_exist_playfile())
                            {
                                emitter_tone_as_musicplay = 0;
                                music_player_play_spec_dev((MUSIC_PLAYER *)(emitter_info.dec_obj), (u32)get_emitter_dev());
                            }
                            else
                            {
                                emitter_tone_as_musicplay = 1;
                                music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_MUSIC_NOFILE_MP3, 0);
                            }
                        }
                        #endif

                    }
                }
                else if(emitter_task_id == TASK_ID_REC)
                {
                    led_fre_set(C_BLED_ON_MODE);
                    emitter_info.dec_obj = emitter_recordfile_play((void*)dev);

					if(emitter_info.dec_obj)
                    {
                        if(back_to_task != emitter_task_id)//
                        {
                            clear_oid_open_bkinfo();
                            emitter_tone_as_musicplay = 2;
                            set_emitter_task_id(emitter_task_id);

                            music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_REC_MP3, 0);
                        }
                        #if 1
                        else
                        {
                            set_emitter_task_id(emitter_task_id);
                            if(get_enter_is_exist_recfile() == 0)
                            {
                                emitter_tone_as_musicplay = 1;
                                music_player_play_path_file(emitter_info.dec_obj, (u8 *)RES_REC_NOFILE_MP3, 0);
                            }
                            else
                            {
                                emitter_tone_as_musicplay = 0;
                                music_player_play_path_file(emitter_info.dec_obj, (u8 *)get_rec_filename(play_rec_file.rec_fname_cnt), 0);
                            }
                        }
                        #endif
                    }
                }

                if(emitter_info.dec_obj == NULL)
                {
                    printf("start_emitter_task_id in,  emitter_info.dec_obj =NULL\n");
                    emitter_media_source(EMITTER_SOURCE_DEC, 0);
                    emitter_task_id++; //next mode.
                    continue;
                }
                else
                {
                    printf("2 emitter_task_id:%d, get_emitter_task_id():%d\n", emitter_task_id, get_emitter_task_id());
                }
            }

            break;


        default:
            {
                if(emitter_task_id >= TASK_ID_MAX)
                    emitter_task_id = TASK_ID_BT;
                else
                    emitter_task_id++;

            }
            break ;
        }

    }
}



/*
 *停止发射
 *(1)退出蓝牙模式
 *(2)发射器主从切换
 *(3)发射器断开连接
 */
void emitter_stop(void)
{
    printf("emitter stop:%x\n", emitter_info.media_source);
    set_emitter_dev(NULL);
    set_emitter_task_id(TASK_ID_BT);
    emitter_media_source_close(emitter_info.media_source);
    emitter_info.media_source = EMITTER_SOURCE_NULL;
    __emitter_send_media_toggle(0);
#if EMITTER_VOL_SYNC
    set_digital_vol(MAX_DIGITAL_VOL_L, MAX_DIGITAL_VOL_R);
#endif
}

/*
 *判断发射器连接信息是否存在
 *1:存在
 *0:不存在
 */
u8 is_emitter_conn_info_exist()
{
    u8 addr[6];
    if (get_tws_db_addr(addr))
    {
        return 1;
    }
    return 0;
}
#endif



