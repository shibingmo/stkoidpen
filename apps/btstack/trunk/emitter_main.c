#include "emitter_main.h"
#include "uart.h"
#include "irq_api.h"
#include "sdk_cfg.h"

#if BT_EMITTER_EN

extern s32 sbc_encode_init(void *pcm_buf, u32 pcm_size, void *sbc_buf, u32 sbc_size);
extern u32 sbc_encode_input(void *pbuf, u32 len, u16 rate);
extern s32 sbc_encode_run(void);
extern s32 sbc_send_run(void);

u16 encode_kick_start_cnt = 0;
volatile u16 encode_kick_start_max = 3;
s32 send_media_to_sbc_encoder(void *pbuf, u32 len, u16 rate, u8 ch)
{
    u8 i;
    s16 tmp_buf[128];
    s16 *ptr;

    if (ch == 2)
    {
        sbc_encode_input(pbuf, len << 1, rate);
    }
    else
    {
        /*mono->stereo*/
        ptr = (s16 *)pbuf;
        for (i = 0 ; i < (len >> 1); i++)
        {
            tmp_buf[i * 2] = ptr[i];
            tmp_buf[i * 2 + 1] = ptr[i];
        }
        sbc_encode_input(tmp_buf, len << 1, rate);
    }

    encode_kick_start_cnt++;
    if (encode_kick_start_cnt >= 3)
    {
        encode_kick_start_cnt = 0;
        irq_set_pending(IRQ_SOFT_ENCODE_IDX);
    }
    return 0;
}

SET_INTERRUPT
s32 media_sbc_encode_loop()
{
    s32 err = 0;
    irq_common_handler(IRQ_SOFT_ENCODE_IDX);
    /*
     *return value define:
     * -1 : no ready
     * -2 : data NULL
     * 0  : send success
     */
    err = sbc_encode_run();
    if (err)
    {
        //printf(" #%x# ",err);
    }
    /*
     *return value define:
     * -1 : no ready
     * -2 : data NULL
     * -3 : send faild
     * 0  : send success
     */
    static u8 send_cnt = 0;
    if (++send_cnt > 3)
    {
        send_cnt = 0;
        err = sbc_send_run();
        if (err)
        {
            //printf(" Err%d ",err);
        }
    }

    return 0;
}

#define PCM_INPUT_BUF_SIZE		(1*1024)
#define SBC_OUTPUT_BUF_SIZE		(1*1024)
static u8 pcm_buffer[PCM_INPUT_BUF_SIZE] ALIGNED(4);
static u8 sbc_buffer[SBC_OUTPUT_BUF_SIZE] ALIGNED(4);
s32 media_sbc_init()
{
    irq_handler_register(IRQ_SOFT_ENCODE_IDX, media_sbc_encode_loop, irq_index_to_prio(IRQ_SOFT_ENCODE_IDX));
    sbc_encode_init(pcm_buffer, PCM_INPUT_BUF_SIZE, sbc_buffer, SBC_OUTPUT_BUF_SIZE);
    puts("media_sbc_init\n");
    return 0;
}

#define SBC_OUTPUT_MAX		3
#define SBC_OUTPUT_TOTAL 	8
#define SBC_OUTPUT_LIMIT	(SBC_OUTPUT_BUF_SIZE*SBC_OUTPUT_MAX/SBC_OUTPUT_TOTAL)
u8 emitter_stream_state(void)
{

#if 0/*sbc info trace*/
    static u16 put_cnt = 0;
    if (put_cnt++ > 1000)
    {
        put_cnt = 0;
        printf("[sbc_size:%d]", get_encoder_outbuf_data_len());
    }
#endif

    if (get_encoder_outbuf_data_len() > SBC_OUTPUT_LIMIT)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

#endif
