#include "headphone.h"
#include "sdk_cfg.h"
#include "common.h"
#include "dac.h"
#include "task_manager.h"
#include "dev_manage.h"
#define HEADPHONE_DBG
#ifdef  HEADPHONE_DBG

#define headphone_printf         log_printf

#else
#define headphone_printf(...)
#endif    //HEADPHONE_DBG

extern u8 get_dac_toggle();

volatile u8 headphone_in_out_flag = 0;


void set_headphone_in_out_flag(u8 flag)
{
    headphone_in_out_flag = flag;
}

u8 get_headphone_in_out_flag(void)
{
    return headphone_in_out_flag;
}


void headphone_inout_detect(void)
{
    static  u8 headphone_in_time = 0;
    static  u8 headphone_out_time = 0;

    CHECK_PIN_IN();

    if( !CHECK_PIN_STATE())
    {
        //headphone_printf("IN %d, %d , %d...\n", headphone_in_time, headphone_out_time, get_dac_toggle());
        headphone_in_time++;
        if(headphone_in_time == 2)//
        {
           set_headphone_in_out_flag(1);
        }
        else
        {
            headphone_out_time = 0;

            SPEAKER_DECODER_LOW();

            if(headphone_in_time>200)
                headphone_in_time = 3;
        }
    }
    else if( CHECK_PIN_STATE() )
    {
        //headphone_printf("OUT %d, %d , %d...\n", headphone_in_time, headphone_out_time, get_dac_toggle());
        headphone_out_time++;
        if(headphone_out_time == 2)//
        {
           set_headphone_in_out_flag(0);
        }
        else
        {
            headphone_in_time = 0;

            if(headphone_out_time>200)
                headphone_out_time = 3;
        }
    }

    return ;
}


LOOP_DETECT_REGISTER(hp_detect) =
{
    .time = 125,
    .fun  = headphone_inout_detect,
};
