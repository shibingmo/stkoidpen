#include "audio/src.h"
#include "audio/audio.h"
#include "circular_buf.h"
#include "uart.h"
#include "sdk_cfg.h"

#ifdef SUPPORT_MS_EXTENSIONS
#pragma bss_seg(	".dac_app_bss")
#pragma data_seg(	".dac_app_data")
#pragma const_seg(	".dac_app_const")
#pragma code_seg(	".dac_app_code")
#endif

u8 src_buffer[SRC_IDAT_LEN_MAX + SRC_ODAT_LEN_MAX + SRC_FLTB_MAX * 48] AT(.dac_buf_sec) __attribute__((aligned(4)));

#define SRC_MODULE_EN	1
#if SRC_MODULE_EN

u8 src_cbuffer[SRC_CBUFF_SIZE] AT(.dac_buf_sec) __attribute__((aligned(4)));
AUDIO_STREAM *src_stream_io = NULL;

#define SRC_OUTPUT_LIMIT	(512*2)

typedef struct __SRC_T_
{
    cbuffer_t src_cbuf;
    u8 clear_flag;
    volatile u8 toggle;
    volatile u8 empty;
    u16 dump_cnt;
    volatile u32 output_free_size;
} SRC_T;
SRC_T src_t;

void src_enable(src_param_t *arg)
{
    u32 size;
    size = src_ops.need_buf(SRC_IDAT_LEN, SRC_ODAT_LEN, SRC_FLTB_MAX);
    printf("src_enable src mem:%d\n", size);
    cbuf_init(&src_t.src_cbuf, src_cbuffer, SRC_CBUFF_SIZE);
    memset(src_buffer, 0, sizeof(src_buffer));
    src_ops.init(arg, src_buffer);
    src_t.toggle = 1;
    src_t.empty = 1;
}

void src_disable()
{
    src_ops.exit(NULL);
    src_t.toggle = 0;
    src_t.empty = 1;
}

u32 src_run(u8 *buf, u16 len)
{
    u32 wlen = 0;
    u8 tmp_buf[SRC_IDAT_LEN];

    if (src_t.toggle)
    {
        if (cbuf_is_write_able(&src_t.src_cbuf, len))
        {
            wlen = cbuf_write(&src_t.src_cbuf, buf, len);
            if (wlen != len)
            {
                putchar('F');
            }
        }

        if (src_t.output_free_size < SRC_OUTPUT_LIMIT)
        {
            //putchar('1');
            return wlen;
        }

        if (src_t.empty)
        {
            if (cbuf_get_data_size(&src_t.src_cbuf) >= SRC_KICK_START_LEN)
            {
                src_t.empty = 0;
                cbuf_read(&src_t.src_cbuf, tmp_buf, SRC_IDAT_LEN);
                //putchar('K');
                src_ops.run(tmp_buf, SRC_IDAT_LEN);
            }
        }
    }
    return wlen;
}

void src_kick_start(u8 start_flag)
{
    u8 tmp_buf[SRC_IDAT_LEN];
    //putchar('k');
    if (src_t.toggle && (src_t.empty == 0))
    {
        if (start_flag)
        {
            src_ops.run(NULL, SRC_IDAT_LEN);
            return;
        }
        if (src_t.output_free_size < SRC_OUTPUT_LIMIT)
        {
            src_t.empty = 1;
            return;
        }
        if (cbuf_get_data_size(&src_t.src_cbuf) >= SRC_IDAT_LEN)
        {
            /* putchar('.'); */
            cbuf_read(&src_t.src_cbuf, tmp_buf, SRC_IDAT_LEN);
            src_ops.run(tmp_buf, SRC_IDAT_LEN);
        }
        else
        {
            src_t.empty = 1;
        }
    }
}

static void src_output_cb(u8 *buf, u16 len, u8 flag)
{
    u32 wlen;
    if (flag & BIT(0))
    {
        if (src_stream_io)
        {
            src_stream_io->output(src_stream_io->priv, buf, len);
            src_t.output_free_size = src_stream_io->free_len(src_stream_io->priv);
        }
        //output
    }

    //kick start
    if (flag == BIT(0))
    {
        src_kick_start(1);/*idat remaind,need not read data */
    }
    else
    {
        src_kick_start(0);/*idat done,need read data again*/
    }
}

void src_clear(void)
{
    memset(src_buffer, 0, sizeof(src_buffer));
    //src_ops.ioctl(CLEAR_SRC_BUF,0,NULL);
}

void src_clear_en(u8 en)
{
    src_t.clear_flag = en;
}

void spdif_src_init()
{
    src_param_t src_parm;

    src_parm.in_chinc = 1;
    src_parm.in_spinc = 2;
    src_parm.out_chinc = 1;
    src_parm.out_spinc = 2;
    src_parm.in_rate = 480;
    src_parm.out_rate = 480;
    src_parm.nchannel = 2;
    src_parm.isr_cb = (void *)src_output_cb;

    AUDIO_STREAM_PARAM stream_param;
    stream_param.ef = AUDIO_EFFECT_NULL;
    stream_param.ch = 2;
    stream_param.sr = SR48000;
    src_stream_io = audio_stream_init(&stream_param, NULL);
    if (src_stream_io)
    {
        src_stream_io->set_sr(src_stream_io->priv, SR48000, 1);
    }

    src_enable(&src_parm);
    //src_run(NULL, 0);
    //src_disable();

}

void spdif_src_set_input_samplerate(u32 sample_rate)
{
    src_param_t src_parm;

    src_parm.in_chinc = 1;
    src_parm.in_spinc = 2;
    src_parm.out_chinc = 1;
    src_parm.out_spinc = 2;
    src_parm.in_rate = sample_rate;  // 882;    // 44100;
    src_parm.out_rate = 480;
    src_parm.nchannel = 2;
    src_parm.isr_cb = (void *)src_output_cb;

    src_ops.ioctl(SET_SRC_PARAM, 0, &src_parm);
}
#endif


#define SRC_CBUFF_MODE	0
#define SRC_DMA_MODE	1
#if BT_EMITTER_EN
#define SRC_MODE	SRC_CBUFF_MODE
#else
#define SRC_MODE	SRC_CBUFF_MODE
#endif

typedef struct
{
    volatile u8 busy;
    u16 in_sr;
    u16 out_sr;
    AUDIO_STREAM input;
    AUDIO_STREAM *output;
} HW_SRC;
HW_SRC hw_src;

static void hw_src_output_cb(u8 *buf, u16 len, u8 flag)
{
    u32 wlen = 0;

    if (flag & BIT(0))
    {
        wlen = hw_src.output->output(hw_src.output->priv, buf, len);
        src_t.output_free_size = hw_src.output->free_len(hw_src.output->priv);

        /* if (wlen != len) {
            putchar('f');
        } */
    }

    if (src_t.clear_flag)
    {
        /*等到输入buf运算完毕再清除*/
        if (flag != BIT(0))
        {
            src_t.clear_flag = 0;
            src_t.empty = 1;
            src_t.dump_cnt = 200;
            //hw_src_init(hw_src.in_sr,hw_src.out_sr);
            src_clear();
#if (SRC_MODE == SRC_CBUFF_MODE)
            cbuf_clear(&src_t.src_cbuf);
#endif
            //printf("data_len:%d\n",hw_src.output->data_len(hw_src.output->priv));
            hw_src.output->clear(hw_src.output->priv);
            return;
        }
    }

#if (SRC_MODE == SRC_CBUFF_MODE)
    if (flag == BIT(0))
    {
        src_kick_start(1);/*idat remaind,need not read data */
    }
    else
    {
        src_kick_start(0);/*idat done,need read data again*/
    }
#else
    if (flag == BIT(0))
    {
        /*src indat remaind,contine*/
        hw_src.busy = 1;
        src_ops.run(NULL, SRC_IDAT_LEN);
    }
    else
    {
        hw_src.busy = 0;
    }
#endif
}

void hw_src_init(u16 in_sr, u16 out_sr)
{
    src_param_t src_parm;

    src_parm.in_chinc = 1;
    src_parm.in_spinc = 2;
    src_parm.out_chinc = 1;
    src_parm.out_spinc = 2;
    src_parm.in_rate = in_sr;
    src_parm.out_rate = out_sr;
    src_parm.nchannel = 2;
    src_parm.isr_cb = (void *)hw_src_output_cb;
    puts("HW_src init\n");
    log_printf("in_sr:%d \nout_sr:%d\n", in_sr, out_sr);

    src_enable(&src_parm);
}
#define EMITTER_SYNC_SRC_STEP	5
#define EMITTER_SYNC_SRC_LIMIT	80
u16 src_sync_sr = 0;
static u32 hw_src_run(void *priv, void *buf, u32 len)
{
    u32 wlen = 0;
    static u16 last_sr = 0;
    src_param_t src_p;
    src_p.in_rate = hw_src.in_sr;

    /* if(last_sr != src_p.in_rate) {
    	printf("src_input_sr:%d\n",src_p.in_rate);
    	last_sr = src_p.in_rate;
    } */

    if (src_t.dump_cnt)
    {
        //putchar('d');
        src_t.dump_cnt--;
        memset(buf, 0, len);
    }

#if BT_EMITTER_EN
    /*emitter stream sync*/
    extern u8 emitter_stream_state(void);
    if (emitter_stream_state())
    {
        src_sync_sr = ((hw_src.out_sr - src_sync_sr) < EMITTER_SYNC_SRC_LIMIT) ? (src_sync_sr - EMITTER_SYNC_SRC_STEP) : (src_sync_sr);
    }
    else
    {
        src_sync_sr = (src_sync_sr < hw_src.out_sr) ? (src_sync_sr + EMITTER_SYNC_SRC_STEP) : (hw_src.out_sr);
    }
#endif
    src_p.out_rate = src_sync_sr;

#if 0
    /* static u16 put_cnt = 0;
    if(put_cnt++ > 500) {
    	put_cnt = 0;
    	printf("<%d>",src_sync_sr);
    } */
    if (src_sync_sr != hw_src.out_sr)
    {
        printf("<sr:%d>", src_sync_sr);
    }
#endif

#if (SRC_MODE == SRC_CBUFF_MODE)
    src_ops.ioctl(SET_SRC_PARAM, 0, &src_p);
    src_t.output_free_size = hw_src.output->free_len(hw_src.output->priv);
    wlen = src_run(buf, len);
    //wlen = hw_src.output->output(hw_src.output->priv, buf, len);
    return wlen;
#else
    if (hw_src.busy)
    {
        //putchar('1');
        return 0;
    }

    if (hw_src.output->free_len(hw_src.output->priv) < SRC_OUTPUT_LIMIT)
    {
        //putchar('2');
        return 0;
    }
    hw_src.busy = 1;

    if (len > SRC_IDAT_LEN)
    {
        wlen = SRC_IDAT_LEN;
    }
    else
    {
        wlen = len;
    }
    src_ops.ioctl(SET_SRC_PARAM, 0, &src_p);
    src_ops.run(buf, wlen);
#endif
    return wlen;
}

static void hw_src_clear(void *priv)
{
    if (hw_src.output)
    {
        hw_src.output->clear(hw_src.output->priv);
    }
}

static tbool hw_src_check(void *priv)
{
    if (hw_src.output)
    {
        return hw_src.output->check(hw_src.output->priv);
    }
    else
    {
        return false;
    }
}
static u32 hw_src_data_len(void *priv)
{
    if (hw_src.output->data_len)
    {
        return hw_src.output->data_len(hw_src.output->priv);
    }
    return 0;
}

static u32 hw_src_free_len(void *priv)
{
    if (hw_src.output->free_len)
    {
        return hw_src.output->free_len(hw_src.output->priv);
    }
    return 0;
}

static void hw_src_samplerate(void *priv, u16 sr, u8 wait)
{
    if (sr)
    {
        hw_src.in_sr = sr;
    }
    if (hw_src.output->set_sr)
    {
        hw_src.output->set_sr(hw_src.output->priv, hw_src.out_sr, wait);
    }
}

AUDIO_STREAM *audio_src_hw_input(AUDIO_STREAM *output, AUDIO_STREAM_PARAM *param, void *priv)
{
    if (output == NULL)
    {
        return NULL;
    }

    memset(&hw_src, 0, sizeof(hw_src));

    hw_src.output 			= output;
    hw_src.input.priv  		= (void *)NULL;
    hw_src.input.output 	= (void *)hw_src_run;
    hw_src.input.clear 		= (void *)hw_src_clear;
    hw_src.input.check 		= (void *)hw_src_check;
    hw_src.input.data_len 	= (void *)hw_src_data_len;
    hw_src.input.free_len 	= (void *)hw_src_free_len;
    hw_src.input.set_sr 	= (void *)hw_src_samplerate;

    hw_src.in_sr = param->sr;
    hw_src.out_sr = *((u16 *)priv);
    src_sync_sr = hw_src.out_sr;
    hw_src_init(param->sr, *((u16 *)priv));

    return &(hw_src.input);
}

