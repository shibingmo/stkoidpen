#include "fs/fs_io.h"
//#include "ffile_io.h"
#include "rec_api.h"
#include "string.h"
#include "rec_file_op.h"
#include "fat_io.h"
#include "file_io.h"
#include "dev_manage.h"

const char rec_folder_name[] = "/XDY_REC";                   //录音文件夹  //仅支持一层目录

#if REC_FILE
const char rec_file_name[] =   "/XDY_REC/FILE0000.MP3";      //MP3录音文件名（含路径）
#else
const char rec_file_name[] =   "/XDY_REC/FILE0000.WAV";      //ADPCM录音文件名（含路径）
#endif
#define REC_MAX_FILENUM    9999   //from 1-9999
#define REC_MAX_FILE_EXIST  10

u32 readyrecnum; //准备要录音的文件号
u32 RecPlayNum;  //播放的文件号
u32 min_rec_num, max_rec_num =0; //录音文件中存在最大和最小的文件号

REC_FILE_INFO last_rec_file;//最新的录音文件
REC_FILE_INFO play_rec_file;//需要播放的录音文件

u32 enter_rec_nofile;  //进入recmode 时检测是否有录音文件

extern char max_rec_file_path[];
static char rec_file_name_last[sizeof(rec_file_name)];

void set_enter_is_exist_recfile(u8 flag)
{
    enter_rec_nofile = flag;
}

u8 get_enter_is_exist_recfile(void)
{
    return enter_rec_nofile;
}

char *get_rec_filename(u32 rec_num)//File does not sure exist
{
    u32 tmp;

    if (rec_num > REC_MAX_FILENUM)
    {
        rec_num = 0;
    }

    memcpy(rec_file_name_last, rec_file_name, sizeof(rec_file_name));
    tmp = rec_num / 1000;

    rec_file_name_last[FILE_NUM_LOCA] = (char)(tmp + '0');

    tmp = rec_num % 1000 / 100;
    rec_file_name_last[FILE_NUM_LOCA + 1] = (char)(tmp + '0');

    tmp = rec_num % 100 / 10;
    rec_file_name_last[FILE_NUM_LOCA + 2] = (char)(tmp + '0');

    tmp = rec_num % 10;
    rec_file_name_last[FILE_NUM_LOCA + 3] = (char)(tmp + '0');

    return rec_file_name_last;
}

static u32 get_recnum_by_filename(char *lastname)//XDY_REC    /FILE0009MP3
{
    u32 rec_num = 0;

    rec_num = (lastname[strlen(lastname)-7]-'0')*1000 + (lastname[strlen(lastname)-6]-'0')*100 + \
              (lastname[strlen(lastname)-5]-'0')*10 + (lastname[strlen(lastname)-4]-'0');
    return rec_num;
}


s16 rec_file_write(FILE_OPERATE *rec_fop_api, u8 *buf, u32 len)
{
    if (rec_fop_api)
    {
        s16 res;
        _FIL_HDL *f_p = file_operate_get_file_hdl(rec_fop_api);
        res = fs_write(f_p, buf, len);
        printf("w");
        return res;
    }
    return -FILE_OP_ERR_OP_HDL;
}

u32 rec_file_tell(FILE_OPERATE *rec_fop_api)
{
    rec_api_printf("--rec_file_seek\n");
    if (rec_fop_api)
    {
        _FIL_HDL *f_p = file_operate_get_file_hdl(rec_fop_api);
        return fs_tell(f_p);
    }
    return -FILE_OP_ERR_OP_HDL;
}

s16 rec_file_close(FILE_OPERATE *rec_fop_api)
{
    //rec_api_printf("--rec_file_close\n");
    if (rec_fop_api)
    {
        u32 file_size;
        _FIL_HDL *f_p = file_operate_get_file_hdl(rec_fop_api);
        fs_sync(f_p);
        file_size = fs_tell(f_p);
       // rec_api_printf("--rec_file_size %d\n", file_size);
        if (file_size <= 512)
        {
            fs_delete(f_p);
        }
        else
        {
            _FIL_HDL *f_p = (_FIL_HDL *)file_operate_get_file_hdl(rec_fop_api);
            FIL *f_h = (FIL *)f_p->hdl;

            last_rec_file.file_number     = file_operate_get_file_number(rec_fop_api);
            last_rec_file.rec_dev         = file_operate_get_dev(rec_fop_api);
            last_rec_file.rec_fname_cnt   = get_readyrecnum();
            last_rec_file.rec_file_sclust = f_h->dir_info.sclust;
            RecPlayNum = get_readyrecnum();
            max_rec_num = get_readyrecnum();

            memcpy(&play_rec_file, &last_rec_file, sizeof(REC_FILE_INFO));
        }
        return fs_close(f_p);
    }
    return -FILE_OP_ERR_OP_HDL;
}

s16 rec_file_seek(FILE_OPERATE *rec_fop_api, u8 type, u32 offsize)
{
    //rec_api_printf("--rec_file_seek\n");
    if (rec_fop_api)
    {
        _FIL_HDL *f_p = file_operate_get_file_hdl(rec_fop_api);
        return fs_seek(f_p, type, offsize);
    }
    return -FILE_OP_ERR_OP_HDL;
}

u32 first_file_num = 0, last_file_num = 0;
u32 set_readyrecnum_by_checklastname(void)
{
    //rec_api_printf("--set_readyrecnum_by_checklastname\n");
    u32 rec_num = 0;

    FILE_OPERATE *fop_api;
    char *f_path = NULL;
    u32 i = 0, ret;

    fop_api = file_operate_creat();

    if (fop_api == NULL)
    {
        printf("creat fop_api err...\n");
        return false;
    }

    file_operate_set_dev(fop_api, (u32)sd0);
    ret = file_api_creat(fop_api->fop_file, (void *)fop_api->fop_info->dev, 0);

    if (ret == false)
    {
        printf("creat file_api err...\n");
        return false;
    }



    file_operate_set_file_sel_mode(fop_api, PLAY_FILE_BYPATH);
    file_operate_set_path(fop_api, (u8*)"/XDY_REC/", 0);
    ret = fs_get_file_bypath(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), (u8 *)fop_api->fop_info->filepath);

    if (!ret)
    {
        fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FOLDER_FILE, &first_file_num, &last_file_num);
       // printf("first_file_num=%d, last_file_num :%d\n", first_file_num, last_file_num);
        fs_close(&(fop_api->fop_file->file_hdl));
        fs_sync(&(fop_api->fop_file->file_hdl));

        for(i = first_file_num; i<=last_file_num; i++)
        {
            fs_get_file_byindex(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), i);
            fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FILE_NAME, &f_path);
            if (f_path)
            {
                rec_num = get_recnum_by_filename(f_path);

               // printf("i=%d name :%s\n",i, f_path);
                if(i == first_file_num)
                {
                    min_rec_num = rec_num;
                    max_rec_num = rec_num;
                }

                if(rec_num<=min_rec_num)
                    min_rec_num = rec_num;
                if(rec_num>=max_rec_num)
                {
                    max_rec_num = rec_num;
                    memcpy(max_rec_file_path, get_rec_filename(max_rec_num), sizeof(rec_file_name));
                }

            }
            fs_close(&(fop_api->fop_file->file_hdl));
            fs_sync(&(fop_api->fop_file->file_hdl));
        }

        rec_num = max_rec_num;
        rec_num++;
        if(rec_num > REC_MAX_FILENUM)
        {
            for(i = 1; i <= (last_file_num-first_file_num+1); i++)
            {

                fs_get_file_byindex(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), first_file_num);//删除一个文件，后面的文件号会往前移动。
                fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FILE_NAME, &f_path);
                //rec_api_printf("--del i:%d --%s\n", i, f_path);
                fs_delete(&(fop_api->fop_file->file_hdl));
                //fs_close(&(fop_api->fop_file->file_hdl));
                //fs_sync(&(fop_api->fop_file->file_hdl));
            }
            rec_num = 1;
            last_file_num = first_file_num =0;
        }
        else if((last_file_num-first_file_num+1) >= REC_MAX_FILE_EXIST)
        {
            rec_api_printf("--del min_rec_num:%s\n", get_rec_filename(min_rec_num));

            fs_get_file_bypath(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), (u8* )get_rec_filename(min_rec_num));
            fs_delete(&(fop_api->fop_file->file_hdl));
            //fs_close(&(fop_api->fop_file->file_hdl));
            //fs_sync(&(fop_api->fop_file->file_hdl));
        }
    }
    else
    {
        //文件夹内为空/
        printf("first_file_num=%d, last_file_num :%d\n", first_file_num, last_file_num);
        rec_api_printf("FOP_OPEN_FILE_BYPATH, no file...\n");
        rec_num = 1;
    }

    file_operate_destroy(&fop_api);

    readyrecnum = rec_num;
    return rec_num;
}

u32 get_readyrecnum(void)
{
    return readyrecnum;
}


bool init_lastandplay_recfile(void)
{
    rec_api_printf("--init_lastandplay_recfile\n");
    u32 rec_num = 0;
    REC_FILE_INFO file_info_tmp;
    FILE_OPERATE *fop_api;
    char *f_path = NULL;
    u32 i = 0, ret;

    fop_api = file_operate_creat();

    if (fop_api == NULL)
    {
        printf("creat fop_api err...\n");
        return false;
    }

    file_operate_set_dev(fop_api, (u32)sd0);
    ret = file_api_creat(fop_api->fop_file, (void *)fop_api->fop_info->dev, 0);

    if (ret == false)
    {
        printf("creat file_api err...\n");
        return false;
    }

    memset(&play_rec_file, 0x00, sizeof(REC_FILE_INFO));
    memset(&last_rec_file, 0x00, sizeof(REC_FILE_INFO));

    set_enter_is_exist_recfile(1);

    file_operate_set_file_sel_mode(fop_api, PLAY_FILE_BYPATH);
    file_operate_set_path(fop_api, (u8*)"/XDY_REC/", 0);
    ret = fs_get_file_bypath(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), (u8 *)fop_api->fop_info->filepath);

    if (!ret)
    {
        fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FOLDER_FILE, &first_file_num, &last_file_num);
        printf("first_file_num=%d, last_file_num :%d\n", first_file_num, last_file_num);
        fs_close(&(fop_api->fop_file->file_hdl));
        fs_sync(&(fop_api->fop_file->file_hdl));

        for(i = first_file_num; i<=last_file_num; i++)
        {
            fs_get_file_byindex(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), i);
            fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FILE_NAME, &f_path);
            if (f_path)
            {
                rec_num = get_recnum_by_filename(f_path);

                printf("i=%d name :%s\n",i, f_path);
                if(i == first_file_num)
                {
                    min_rec_num = rec_num;
                    max_rec_num = rec_num;
                }

                if(rec_num<=min_rec_num)
                    min_rec_num = rec_num;
                if(rec_num>=max_rec_num)
                {
                    max_rec_num = rec_num;
                    memcpy(max_rec_file_path, get_rec_filename(max_rec_num), sizeof(rec_file_name));
                }
            }
            fs_close(&(fop_api->fop_file->file_hdl));
            fs_sync(&(fop_api->fop_file->file_hdl));
        }

        file_operate_set_path(fop_api, (void *)get_rec_filename(max_rec_num), 0);
        ret = file_operate_op(fop_api, FOP_OPEN_FILE_BYPATH, NULL, NULL);
        if (ret)   //err
        {
            rec_api_printf("init_lastandplay_recfile open err, max_rec_num=%d ...\n", max_rec_num);
        }
        else
        {
            if(rec_get_file_info(fop_api, &file_info_tmp) == true)
            {
                memcpy(&last_rec_file, &file_info_tmp, sizeof(REC_FILE_INFO));
                memcpy(&play_rec_file, &file_info_tmp, sizeof(REC_FILE_INFO));
            }
        }
    }
    else
    {
        //文件夹内为空/
        printf("no rec file OR no rec folder\n");
        set_enter_is_exist_recfile(0);
    }

    file_operate_destroy(&fop_api);

    return true;
}

tbool is_exist_recfile_bypath(FILE_OPERATE *fop_api, u8 *path)
{
    u32 ret;

    set_enter_is_exist_recfile(0);

    if (fop_api == NULL)
    {
        rec_api_printf("fop_api == NULL\n");
        return false ;
    }


    file_operate_dev_bp(fop_api, NULL);
    file_operate_set_path(fop_api, path, 0);

    ret = fs_get_file_bypath(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), (u8 *)fop_api->fop_info->filepath);
    if(!ret)
    {
        rec_api_printf("is_exist_recfile_bypath: exist rec file.\n");
        fs_close(&(fop_api->fop_file->file_hdl));
        set_enter_is_exist_recfile(1);
        return true;
    }
    else
    {
        rec_api_printf("is_exist_recfile_bypath: no rec file.\n");
        return false;
    }
}




void delete_recfile_by_index(FILE_OPERATE *rec_fop_api)
{
    _FIL_HDL f_p;

    char *file_name =NULL;

    if((last_file_num-first_file_num+1) >= REC_MAX_FILE_EXIST)
    {
        file_name = get_rec_filename(min_rec_num);
        rec_api_printf("--del file_name:%s\n", file_name);
        fs_get_file_bypath(&(rec_fop_api->fop_file->fs_hdl), &f_p, (u8* )file_name);
        fs_delete(&f_p);
    }
}

s16 rec_file_open(FILE_OPERATE *rec_fop_api)//creat recode new file name
{
    rec_api_printf("--rec_file_open\n");

    if (rec_fop_api)
    {
        s16 ret =1;
        _FIL_HDL *f_p = file_operate_get_file_hdl(rec_fop_api);
        _FS_HDL  *fs_p = file_operate_get_fs_hdl(rec_fop_api);

        char *file_name =NULL;

        file_name =  get_rec_filename(get_readyrecnum());

        ret = fs_open(fs_p, f_p, file_name, FA_CREATE_NEW | FA_WRITE);
        if (ret == FR_OK)
        {
            printf("--rec_file_new:%s\n", file_name);
            memcpy(max_rec_file_path, file_name, sizeof(rec_file_name));
            fs_sync(f_p);
            return FR_OK;
        }
        else   //其他错误
        {
            rec_api_printf("--open rec file err:%d\n", ret);
            return ret;
        }
    }
    return -FILE_OP_ERR_OP_HDL;
}

s16 rec_fs_open(FILE_OPERATE **fop_api_p, void *rec_dev)
{
    u32 err = 0;
    FILE_OPERATE *fop_api;

    if (fop_api_p == NULL)
    {
        return -FILE_OP_ERR_OP_HDL;
    }


    rec_api_printf("fun = %s, line = %d\n", __func__, __LINE__);
    *fop_api_p = file_operate_creat();
    if (*fop_api_p == NULL)
    {
        return -FILE_OP_ERR_NO_MEM;
    }

    fop_api = *fop_api_p;
    rec_api_printf("fun = %s, line = %d\n", __func__, __LINE__);
    if (rec_dev == NULL)
    {
        file_operate_set_dev_sel_mode(fop_api, DEV_SEL_FIRST);
    }
    else
    {
        file_operate_set_dev_sel_mode(fop_api, DEV_SEL_SPEC);
        file_operate_set_dev(fop_api, (u32)rec_dev);
    }
    err = file_operate_dev_sel(fop_api);
    if (err)
    {
        rec_api_printf("rec dev sel err = %x\n", err);
        return -err;
    }

    err = file_operate_rec_file_api_creat(fop_api);
    rec_api_printf("fun = %s, line = %d\n", __func__, __LINE__);
    if (!err)
    {
        s16 make_dir_status;
        make_dir_status = file_operate_op(fop_api, FOP_CREAT_FOLDER, (void *)rec_folder_name, NULL);
        if (make_dir_status < 0)
        {
            rec_api_printf("FILE_OP_ERR_NOT_INIT\n");
            return -FILE_OP_ERR_NOT_INIT;
        }
        else
        {
            if (FR_EXIST == make_dir_status)
            {
                rec_api_printf("FR_EXIST\n");
                return FR_EXIST;
            }
            rec_api_printf("make_dir_status\n");
            return make_dir_status;
        }
    }

    return FR_EXIST;
}

void rec_fs_close(FILE_OPERATE **fop_api_p)
{

    if (fop_api_p)
    {
        rec_api_printf("fun = %s, line = %d\n", __func__, __LINE__);
        file_operate_destroy(fop_api_p);
    }
}


bool rec_get_file_info(void *fop_api, REC_FILE_INFO *rec_file_info)//current open file info.
{
    REC_FILE_INFO file_info_tmp;
    unsigned char *path = NULL;

    if (fop_api == NULL)
    {
        return false;
    }

    memset(&file_info_tmp, 0x00, sizeof(REC_FILE_INFO));

    file_info_tmp.rec_dev = file_operate_get_dev(fop_api);
    file_info_tmp.file_number = file_operate_get_file_number(fop_api);
    //file_operate_get_file_name(fop_api, path);
    //path =  ((FILE_OPERATE *)fop_api)->fop_info->filepath;
    fs_io_ctrl(NULL, &(((FILE_OPERATE*)fop_api)->fop_file->file_hdl), FS_IO_GET_FILE_NAME, &path);
    if(path)
    {
        file_info_tmp.rec_fname_cnt = get_recnum_by_filename((char *)path);
    }


    _FIL_HDL *f_p = (_FIL_HDL *)file_operate_get_file_hdl(fop_api);
    FIL *f_h = (FIL *)f_p->hdl;
    file_info_tmp.rec_file_sclust = f_h->dir_info.sclust;

    if (file_info_tmp.rec_file_sclust == 0)
    {
        return false;
    }

    memcpy(rec_file_info, &file_info_tmp, sizeof(REC_FILE_INFO));

    return true;
}

#if REC_DEL
bool rec_fop_update_lastfile(REC_FILE_INFO *rec_file_info)
{
    rec_api_printf("--rec_fop_update_lastfile\n");
    u32 rec_num = 0;

    REC_FILE_INFO file_info_tmp;
    FILE_OPERATE *fop_api;
    char *f_path = NULL;
    u32 i = 0, ret;

    fop_api = file_operate_creat();

    if (fop_api == NULL)
    {
        printf("creat fop_api err...\n");
        return false;
    }

    file_operate_set_dev(fop_api, (u32)sd0);
    ret = file_api_creat(fop_api->fop_file, (void *)fop_api->fop_info->dev, 0);

    if (ret == false)
    {
        printf("creat file_api err...\n");
        return false;
    }

    memset(&file_info_tmp, 0x00, sizeof(REC_FILE_INFO));

    file_operate_set_file_sel_mode(fop_api, PLAY_FILE_BYPATH);
    file_operate_set_path(fop_api, (u8*)"/XDY_REC/", 0);
    ret = fs_get_file_bypath(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), (u8 *)fop_api->fop_info->filepath);

    if (!ret)
    {
        fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FOLDER_FILE, &first_file_num, &last_file_num);
        printf("first_file_num=%d, last_file_num :%d\n", first_file_num, last_file_num);
        fs_close(&(fop_api->fop_file->file_hdl));
        fs_sync(&(fop_api->fop_file->file_hdl));

        for(i = first_file_num; i<=last_file_num; i++)
        {
            fs_get_file_byindex(&(fop_api->fop_file->fs_hdl), &(fop_api->fop_file->file_hdl), i);
            fs_io_ctrl(NULL, &(fop_api->fop_file->file_hdl), FS_IO_GET_FILE_NAME, &f_path);
            if (f_path)
            {
                rec_num = get_recnum_by_filename(f_path);

                printf("i=%d name :%s\n",i, f_path);
                if(i == first_file_num)
                {
                    min_rec_num = rec_num;
                    max_rec_num = rec_num;
                }

                if(rec_num<=min_rec_num)
                    min_rec_num = rec_num;

                if(rec_num>=max_rec_num)
                {
                    max_rec_num = rec_num;
                    memcpy(max_rec_file_path, get_rec_filename(max_rec_num), sizeof(rec_file_name));

                    file_info_tmp.rec_dev = file_operate_get_dev(fop_api);
                    file_info_tmp.file_number = file_operate_get_file_number(fop_api);
                    file_info_tmp.rec_fname_cnt = max_rec_num;

                    _FIL_HDL *f_p = (_FIL_HDL *)file_operate_get_file_hdl(fop_api);
                    FIL *f_h = (FIL *)f_p->hdl;
                    file_info_tmp.rec_file_sclust = f_h->dir_info.sclust;

                    memcpy(rec_file_info, &file_info_tmp, sizeof(REC_FILE_INFO));
                }
            }
            fs_close(&(fop_api->fop_file->file_hdl));
            fs_sync(&(fop_api->fop_file->file_hdl));
        }
    }
    else
    {
        //文件夹内为空/
        file_operate_destroy(&fop_api);
        return false;
    }

    file_operate_destroy(&fop_api);
    return  true;
}

u32 rec_fop_update_playfile(REC_FILE_INFO *rec_file_info, u8 flag)
{
    FILE_OPERATE *rec_fop_api;
    s16 ret;
    u32 TemRecPlayNum = 0;



    rec_api_printf("play_rec_file.rec_fname_cnt:%d\n", play_rec_file.rec_fname_cnt);
    ret = rec_fs_open(&rec_fop_api, (void *)sd0);
    if ((ret != FR_EXIST) && (ret != FR_OK))
    {
        rec_api_printf("FR_NO_FILESYSTEM...\n");
        return FR_NO_FILESYSTEM;
    }

    TemRecPlayNum = rec_file_info->rec_fname_cnt;

    do
    {
        if(flag)
        {
            TemRecPlayNum++;
            if(TemRecPlayNum > max_rec_num)
                TemRecPlayNum = min_rec_num;
        }
        else
        {
            if(TemRecPlayNum)
            {
                TemRecPlayNum--;

                if((TemRecPlayNum == 0) || (TemRecPlayNum < min_rec_num))
                    TemRecPlayNum = max_rec_num;
            }
            else
            {
                TemRecPlayNum = max_rec_num;
            }
        }

        file_operate_set_path(rec_fop_api, (void *)get_rec_filename(TemRecPlayNum), 0);
        ret = file_operate_op(rec_fop_api, FOP_OPEN_FILE_BYPATH, NULL, NULL);
        if (ret)   //err
        {
            rec_api_printf("rec_fop_update_playfile open err, TemRecPlayNum=%d ...\n", TemRecPlayNum);
        }
        else
        {
            rec_api_printf("rec_get_file_info...\n");
            if(rec_get_file_info(rec_fop_api, rec_file_info) == true)
            {
                RecPlayNum = TemRecPlayNum;
                rec_file_info->rec_fname_cnt = TemRecPlayNum;
            }
        }
    }
    while (ret);
    rec_fs_close(&rec_fop_api);
    return FR_OK;
}



u32 rec_fop_del_recfile(REC_FILE_INFO *rec_file_info)
{
    FILE_OPERATE *rec_fop_api;
    s16 ret;
    if (rec_file_info == NULL)
    {
        return FR_FILE_ERR;
    }

    ret = rec_fs_open(&rec_fop_api, rec_file_info->rec_dev);
    if ((ret != FR_EXIST) && (ret != FR_OK))
    {
        return FR_NO_FILESYSTEM;
    }

    file_operate_set_file_sclust(rec_fop_api, rec_file_info->rec_file_sclust);
    ret = file_operate_op(rec_fop_api, FOP_OPEN_FILE_BYSCLUCT, NULL, NULL);
    if (ret)
    {
        ret = FR_NO_FILE;
        goto exit;
    }

    ret = file_operate_op(rec_fop_api, FOP_DEL_FILE, NULL, NULL);
    rec_file_info->rec_file_sclust = 0;

exit:

    is_exist_recfile_bypath(rec_fop_api, (u8 *) "/XDY_REC/");

    rec_fs_close(&rec_fop_api);
    return ret;
}

#endif
void *rec_fop_get_dev(FILE_OPERATE *fop_api)
{
    return file_operate_get_dev(fop_api);
}
